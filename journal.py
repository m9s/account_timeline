#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.pool import Pool


class JournalAccount(ModelSQL, ModelView):
    'Journal Account'
    _name = 'account.journal.account'
    _description = __doc__
    _rec_name = 'type'

    type = fields.Selection([
            ('credit_account', 'Credit Account'),
            ('debit_account', 'Debit Account')
            ], 'Type', required=True)
    account = fields.Many2One('account.account', 'Account', required=True,
            domain=[
                ('company', '=', Eval('company')),
                ('fiscalyear', '=', Eval('fiscalyear')),
                ('kind', '!=', 'view'),
            ], depends=['company', 'fiscalyear'])
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscalyear',
            required=True)
    journal = fields.Many2One('account.journal', 'Journal', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)

    def default_company(self):
        return Transaction().context.get('company', False)

JournalAccount()


class Journal(ModelSQL, ModelView):
    _name = 'account.journal'

    journal_accounts = fields.One2Many('account.journal.account', 'journal',
            'Default Journal Accounts')

    def __init__(self):
        super(Journal, self).__init__()
        self._constraints += [
            ('check_unique_default_account_fiscalyear',
             'one_account_per_type_and_fiscalyear'),
        ]
        self._error_messages.update({
            'one_account_per_type_and_fiscalyear': 'There can be only one '
                    'account per type and fiscalyear!',
            })
        self.credit_account = copy.copy(self.credit_account)
        self.credit_account = fields.Function(fields.Many2One('account.account',
                'Credit Account'), 'get_account')

        self.debit_account = copy.copy(self.debit_account)
        self.debit_account = fields.Function(fields.Many2One('account.account',
                'Debit Account'), 'get_account')
        self._reset_columns()

    def get_account(self, ids, name):
        pool = Pool()
        fiscalyear_obj = pool.get('account.fiscalyear')
        journal_account_obj = pool.get('account.journal.account')
        user_obj = pool.get('res.user')

        res = {}

        type = None
        if 'credit_account' in name:
            type = 'credit_account'
        if 'debit_account' in name:
            type = 'debit_account'

        if Transaction().context.get('company'):
            company_id = Transaction().context['company']
        else:
            user2 = user_obj.browse(Transaction().user)
            if user2.company:
                company_id = user2.company.id
            else:
                for id in ids:
                    res[id] = []
                return res

        fiscalyear = fiscalyear_obj.get_fiscalyear(
                date=Transaction().context.get('effective_date'))

        for id in ids:
            args = [
                ('type', '=', type),
                ('fiscalyear', '=', fiscalyear and fiscalyear.id or False),
                ('journal', '=', id),
                ('company', '=', company_id)
                ]
            account_ids = journal_account_obj.search(args)
            if account_ids:
                account_rec = journal_account_obj.browse(account_ids[0])
                res[id] = account_rec.account.id
            else:
                res[id] = []
        return res

    def check_unique_default_account_fiscalyear(self, ids):
        res = True
        res2 = {}
        for journal in self.browse(ids):
            for account in journal.journal_accounts:
                if res2.has_key((account.type, account.fiscalyear.id)):
                    res = False
                else:
                    res2[(account.type, account.fiscalyear.id)] = 1
        return res

Journal()
