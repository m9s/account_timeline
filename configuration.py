#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Account Tax Rule Configuration'
    _name = 'account.tax.rule.configuration'
    _description = __doc__

    default_customer_tax_rule = fields.Property(
        fields.Many2One('account.tax.rule', 'Default Customer Tax Rule ')
        )
    default_supplier_tax_rule = fields.Property(
        fields.Many2One('account.tax.rule', 'Default Supplier Tax Rule')
        )
Configuration()
