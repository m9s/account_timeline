#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.pool import Pool


class Party(ModelSQL, ModelView):
    _name = 'party.party'

    accounts = fields.One2Many('party.account', 'party', 'Accounts')

    def __init__(self):
        super(Party, self).__init__()
        self.account_payable = copy.copy(self.account_payable)
        self.account_payable = fields.Function(fields.Many2One(
                'account.account', 'Account Payable'), 'get_account')
        self.account_receivable = copy.copy(self.account_receivable)
        self.account_receivable = fields.Function(fields.Many2One(
                'account.account', 'Account Receivable'), 'get_account')

        self._reset_columns()

    def default_accounts(self):
        fiscalyear_obj = Pool().get('account.fiscalyear')

        res = []

        fiscalyear_ids = fiscalyear_obj.search([])
        if fiscalyear_ids:
            fiscalyears = fiscalyear_obj.browse(fiscalyear_ids)

            for year in fiscalyears:
                res.append({
                    'type': 'receivable',
                    'fiscalyear': year.id,
                    'account': year.default_account_receivable.id})
                res.append({
                    'type': 'payable',
                    'fiscalyear': year.id,
                    'account': year.default_account_payable.id})
        return res

    def get_account(self, ids, names):
        pool = Pool()
        fiscalyear_obj = pool.get('account.fiscalyear')
        party_account_obj = pool.get('party.account')

        res = {}

        fiscalyear_id = fiscalyear_obj.get_fiscalyear_id(
                Transaction().context.get('effective_date'))
        for name in names:
            res.setdefault(name, {})
            if fiscalyear_id:
                args = [
                    ('type', '=', name.replace('account_', '')),
                    ('fiscalyear', '=', fiscalyear_id),
                    ('party', 'in', ids),
                    ]
                party_account_ids = party_account_obj.search(args)
                party_accounts = party_account_obj.browse(party_account_ids)
                for party_account in party_accounts:
                    res[name][party_account.party.id] = party_account.account.id

            for party_id in ids:
                res[name].setdefault(party_id, False)
        return res

Party()


class PartyAccount(ModelSQL, ModelView):
    'Party Account'
    _name = 'party.account'
    _description = __doc__
    _rec_name = 'party'

    party = fields.Many2One('party.party', 'Party', required=True,
            ondelete='CASCADE', select=1)
    type = fields.Selection([
        ('receivable', 'Account Receivable'),
        ('payable', 'Account Payable')
        ], 'Type', required=True, select=1)
    account = fields.Many2One('account.account', 'Account', required=True,
            domain=[
                ('fiscalyear', '=', Eval('fiscalyear')),
                ('kind', '=', Eval('type'))
                ],
            depends=['fiscalyear', 'type'])
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True, select=1)

    def __init__(self):
        super(PartyAccount, self).__init__()
        self._order.insert(0, ('fiscalyear', 'DESC'))
        self._sql_constraints += [
            ('journal_period_uniq', 'UNIQUE(party, type, account, fiscalyear)',
                'There can be only one payable and one receivable account '
                'per fiscalyear!'),
            ]

    def default_type(self):
        return 'receivable'

    def default_fiscalyear(self):
        fiscalyear_obj = Pool().get('account.fiscalyear')
        return fiscalyear_obj.get_fiscalyear_id()

PartyAccount()
