#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
import datetime
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.backend import TableHandler
from trytond.pyson import If, Eval, In, Get, PYSONEncoder
from trytond.transaction import Transaction
from trytond.pool import Pool

from .fiscalyear import _TAXATION_METHODS

class TaxAccountTemplate(ModelSQL, ModelView):
    'Tax Account Template'
    _name = 'account.tax.tax.account.template'
    _description = __doc__
    _rec_name = 'account'

    type = fields.Selection([
        ('invoice_account', 'Invoice Account'),
        ('credit_note_account', 'Credit Note Account'),
        ], 'Type', required=True)
    tax = fields.Many2One('account.tax.template', 'Tax', required=True)
    account = fields.Many2One('account.account.template', 'Account',
            required=True)
    account_template = fields.Many2One('account.account.template',
            'Account Template', required=True)

    def create_tax_account(self, template, fiscalyear_id, template2account,
            template2tax, template2tax_account=None):
        '''
        Create recursively tax accounts based on template.

        :param template: the template id or the BrowseRecord of template
                used for tax creation
        :param company_id: the id of the company for which taxes are created
        :param template2tax_code: a dictionary with tax code template id as key
                and tax code id as value, used to convert tax code template into
                tax code
        :param template2account: a dictionary with account template id as key
                and account id as value, used to convert account template into
                account code
        :param template2tax: a dictionary with tax template id as key and
                tax id as value, used to convert template id into tax.
                The dictionary is filled with new taxes
        :return: id of the tax account created
        '''
        tax_account_obj = Pool().get('tax.account')
        lang_obj = Pool().get('ir.lang')

        if template2tax_account is None:
            template2tax_account = {}

        if isinstance(template, (int, long)):
            template = self.browse(template)

        if template.id not in template2tax_account:
            vals = self._get_tax_account_value(template)
            vals['fiscalyear'] = fiscalyear_id
            vals['account'] = template2account[template.account.id]
            vals['tax'] = template2tax[template.tax.id]

            new_id = tax_account_obj.create(vals)

            prev_lang = template._context.get('language') or 'en_US'
            for lang in lang_obj.get_translatable_languages():
                if lang == prev_lang:
                    continue
                template.setLang(lang)
                data = {}
                for field in template._columns.keys():
                    if getattr(template._columns[field], 'translate', False):
                        data[field] = template[field]
                if data:
                    with Transaction().set_context(language=lang):
                        tax_account_obj.write(new_id, data)
            template.setLang(prev_lang)
            template2tax_account[template.id] = new_id
        else:
            new_id = template2tax_account[template.id]

        return new_id

    def _get_tax_account_value(self, template, tax_account=None):
        '''
        Set values for tax account creation.

        :param template: the BrowseRecord of the template
        :param tax_account: the BrowseRecord of the tax account to update
        :return: a dictionary with tax account fields as key and values as value
        '''
        res = {}
        for field in ('type',):
            if not tax_account or tax_account[field] != template[field]:
                res[field] = template[field]
        if not tax_account or tax_account.template.id != template.id:
            res['template'] = template.id
        return res

TaxAccountTemplate()


class TaxAccount(ModelSQL, ModelView):
    'Tax Account'
    _name = 'tax.account'
    _description = __doc__
    _rec_name = 'tax'

    type = fields.Selection([
        ('invoice_account', 'Invoice Account'),
        ('credit_note_account', 'Credit Note Account'),
        ], 'Type', required=True, select=1)
    tax = fields.Many2One('account.tax', 'Tax', required=True)
    account = fields.Many2One('account.account', 'Account', required=True,
            domain=[('fiscalyear', '=', Eval('fiscalyear'))],
            depends=['fiscalyear'])
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True)
    template = fields.Many2One('account.tax.tax.account.template',
            'Tax Account Template')

    def default_fiscalyear(self):
        fiscalyear_obj = Pool().get('account.fiscalyear')
        date_obj = Pool().get('ir.date')

        res = False

        today = date_obj. today()
        args = [
            ('start_date', '<', today),
            ('end_date', '>', today)
            ]
        fiscalyear_ids = fiscalyear_obj.search(args)
        if fiscalyear_ids:
            res = fiscalyear_ids[0]
        return res

    def default_type(self):
        return 'invoice_account'

    def update_tax_account(self, tax_account, fiscalyear_id,
            template2account, template2tax,
            template2tax_account=None):
        '''
        Update recursively tax accounts based on template.

        :param template: the template id or the BrowseRecord of template
                used for tax creation
        :param template2tax_code: a dictionary with tax code template id as key
                and tax code id as value, used to convert tax code template into
                tax code
        :param template2account: a dictionary with account template id as key
                and account id as value, used to convert account template into
                account code
        :param template2tax: a dictionary with tax template id as key and
                tax id as value, used to convert template id into tax.
                The dictionary is filled with new taxes
        '''
        tax_account_template_obj = Pool().get('account.tax.tax.account.template')
        lang_obj = Pool().get('ir.lang')

        if template2tax_account is None:
            template2tax_account = {}

        if isinstance(tax_account, (int, long)):
            tax_account = self.browse(tax_account)

        if tax_account.template:
            vals = tax_account_template_obj._get_tax_account_value(
                    tax_account.template,
                    tax_account=tax_account)
            vals['fiscalyear'] = fiscalyear_id
            vals['account'] = template2account[tax_account.template.account.id]
            vals['tax'] = template2tax[tax_account.template.tax.id]

            if vals:
                self.write(tax_account.id, vals)

            prev_lang = tax_account._context.get('language') or 'en_US'
            for lang in lang_obj.get_translatable_languages():
                if lang == prev_lang:
                    continue
                tax_account.setLang(lang)
                with Transaction().set_context(language=lang):
                    data = tax_account_template_obj._get_tax_account_value(
                            tax_account.template, tax_account=tax_account)
                    if data:
                        self.write(tax_account.id, data)
            tax_account.setLang(prev_lang)
            template2tax_account[tax_account.template.id] = tax_account.id

TaxAccount()


class TaxCodeTemplate(ModelSQL, ModelView):
    'Tax Tax Code Template'
    _name = 'account.tax.tax.code.template'
    _description = __doc__

    type = fields.Selection([
        ('invoice_base', 'Invoice Base'),
        ('invoice_tax', 'Invoice Tax'),
        ('credit_note_base', 'Credit Note Base'),
        ('credit_note_tax', 'Credit Note Tax'),
        ], 'Type', required=True)
    sign = fields.Numeric('Sign', digits=(2, 0),
            help='Usualy 1 or -1',
            required=True)
    tax = fields.Many2One('account.tax.template', 'Tax', required=True)
    code = fields.Many2One('account.tax.code.template', 'Tax Code',
            required=True, domain=[('type', '=',
                If(In(Eval('type'),
                    ['invoice_base', 'credit_note_base']), 'base',
                    If(In(Eval('type'),
                        ['invoice_tax', 'credit_note_tax']), 'tax', ''))),
                ['OR', ('valid_from', '<=', Eval('valid_from')),
                    ('valid_from', '=', False)],
                ['OR', ('valid_to', '>=', Eval('valid_to')),
                    ('valid_to', '=', False)],
                ],
            depends=['type', 'valid_from', 'valid_to'])
    valid_from = fields.Date('Valid from')
    valid_to = fields.Date('Valid to')
    account_template = fields.Many2One('account.account.template',
            'Account Template', required=True)

    def create_tax_tax_code(self, template, template2tax_code,
            template2tax, template2tax_tax_code=None):
        '''
        Create recursively tax tax codes based on template.

        :param template: the template id or the BrowseRecord of template
                used for tax tax code creation
        :param template2tax_code: a dictionary with tax code template id as key
                and tax code id as value, used to convert tax code template into
                tax code
        :param template2account: a dictionary with account template id as key
                and account id as value, used to convert account template into
                account code
        :param template2tax: a dictionary with tax template id as key and
                tax id as value, used to convert template id into tax.
                The dictionary is filled with new taxes
        :return: id of the tax account created
        '''
        tax_tax_code_obj = Pool().get('tax.code')
        lang_obj = Pool().get('ir.lang')

        if template2tax_tax_code is None:
            template2tax_tax_code = {}

        if isinstance(template, (int, long)):
            template = self.browse(template)

        if template.id not in template2tax_tax_code:
            vals = self._get_tax_tax_code_value(template,
                )
            vals['code'] = template2tax_code[template.code.id]
            vals['tax'] = template2tax[template.tax.id]

            new_id = tax_tax_code_obj.create(vals)

            prev_lang = template._context.get('language') or 'en_US'
            for lang in lang_obj.get_translatable_languages():
                if lang == prev_lang:
                    continue
                template.setLang(lang)
                data = {}
                for field in template._columns.keys():
                    if getattr(template._columns[field], 'translate', False):
                        data[field] = template[field]
                if data:
                    with Transaction().set_context(language=lang):
                        tax_tax_code_obj.write(new_id, data)
            template.setLang(prev_lang)
            template2tax_tax_code[template.id] = new_id
        else:
            new_id = template2tax_tax_code[template.id]

        return new_id

    def _get_tax_tax_code_value(self, template, tax_tax_code=None):
        '''
        Set values for tax tax code creation.

        :param template: the BrowseRecord of the template
        :param tax_tax_code: the BrowseRecord of the tax tax code to update
        :return: a dictionary with tax tax code fields as key and values as value
        '''
        res = {}
        for field in ('type', 'sign', 'valid_from', 'valid_to'):
            if not tax_tax_code or tax_tax_code[field] != template[field]:
                res[field] = template[field]
        if not tax_tax_code or tax_tax_code.template.id != template.id:
            res['template'] = template.id
        return res

TaxCodeTemplate()


class TaxCode(ModelSQL, ModelView):
    'Tax Tax Code'
    _name = 'tax.code'
    _description = __doc__
    _rec_name = 'tax'

    type = fields.Selection([
        ('invoice_base', 'Invoice Base'),
        ('invoice_tax', 'Invoice Tax'),
        ('credit_note_base', 'Credit Note Base'),
        ('credit_note_tax', 'Credit Note Tax'),
        ], 'Type', required=True, select=1)
    sign = fields.Numeric('Sign', digits=(2, 0),
            help='Usualy 1 or -1',
            required=True)
    tax = fields.Many2One('account.tax', 'Tax', required=True)
    code = fields.Many2One('account.tax.code', 'Tax Code',
            required=True, domain=[('type', '=',
                If(In(Eval('type'),
                    ['invoice_base', 'credit_note_base']), 'base',
                    If(In(Eval('type'),
                        ['invoice_tax', 'credit_note_tax']), 'tax', ''))),
                ['OR', ('valid_from', '<=', Eval('valid_from')),
                    ('valid_from', '=', False)],
                ['OR', ('valid_to', '>=', Eval('valid_to')),
                    ('valid_to', '=', False)],
                ],
            depends=['type', 'valid_from', 'valid_to'])
    valid_from = fields.Date('Valid from')
    valid_to = fields.Date('Valid to')
    template = fields.Many2One('account.tax.tax.code.template',
            'Tax Code Template')

    def default_sign(self):
        return Decimal('1')

    def default_type(self):
        return 'invoice_base'

    def update_tax_tax_code(self, tax_tax_code, template2tax_code,
            template2tax, template2tax_tax_code=None):
        '''
        Update recursively tax tax codes based on template.

        :param template: the template id or the BrowseRecord of template
                used for tax tax code update
        :param template2tax_code: a dictionary with tax code template id as key
                and tax code id as value, used to convert tax code template into
                tax code
        :param template2account: a dictionary with account template id as key
                and account id as value, used to convert account template into
                account code
        :param template2tax: a dictionary with tax template id as key and
                tax id as value, used to convert template id into tax.
                The dictionary is filled with new taxes
        :return: id of the tax account created
        '''
        tax_tax_code_template_obj = Pool().get('account.tax.tax.code.template')
        lang_obj = Pool().get('ir.lang')

        if template2tax_tax_code is None:
            template2tax_tax_code = {}

        if isinstance(tax_tax_code, (int, long)):
            tax_tax_code = self.browse(tax_tax_code)

        if tax_tax_code.template.id:
            vals = tax_tax_code_template_obj._get_tax_tax_code_value(
                    tax_tax_code.template)
            vals['code'] = template2tax_code[tax_tax_code.template.code.id]
            vals['tax'] = template2tax[tax_tax_code.template.tax.id]

            if vals:
                self.write(tax_tax_code.id, vals)

            prev_lang = tax_tax_code._context.get('language') or 'en_US'
            for lang in lang_obj.get_translatable_languages():
                if lang == prev_lang:
                    continue
                tax_tax_code.setLang(lang)
                with Transaction().set_context(language=lang):
                    data = tax_tax_code_template_obj._get_tax_tax_code_value(
                            tax_tax_code.template)
                    if data:
                        self.write(tax_tax_code.id, data)
            tax_tax_code.setLang(prev_lang)
            template2tax_tax_code[tax_tax_code.template.id] = tax_tax_code.id

TaxCode()


class OpenChartCode(Wizard):
    _name = 'account.tax.open_chart_code'

    def _action_open_chart(self, data):
        fiscalyear_obj = Pool().get('account.fiscalyear')
        period_obj = Pool().get('account.period')

        res = super(OpenChartCode, self)._action_open_chart(data)

        if data['form']['method'] == 'fiscalyear':
            fiscalyear = fiscalyear_obj.browse(data['form']['fiscalyear'])
            start_date = fiscalyear.start_date
            end_date = fiscalyear.end_date
        else:
            start_date = datetime.date.min
            end_date = datetime.date.max
            for period in period_obj.browse(
                    data['form']['periods'][0][1]):
                if start_date == datetime.date.min:
                    start_date = period.start_date
                if end_date == datetime.date.max:
                    end_date = period.end_date
                if period.start_date < start_date:
                    start_date = period.start_date
                if period.end_date > end_date:
                    end_date = period.end_date

        domain = [
                ['OR', ('valid_from', '<=', end_date),
                    ('valid_from', '=', False)],
                ['OR', ('valid_to', '>=', start_date),
                    ('valid_to', '=', False)]
                ]
        pyson_domain = PYSONEncoder().encode(domain)
        res['pyson_domain'] = res['pyson_domain'][:-1] + ", " + \
                pyson_domain[1:-1] + "]"
        return res

OpenChartCode()


class CodeTemplate(ModelSQL, ModelView):
    _name = 'account.tax.code.template'

    valid_from = fields.Date('Valid from')
    valid_to = fields.Date('Valid to')
    description = fields.Text('Description')

    def _get_tax_code_value(self, template, code=None):
        res = super(CodeTemplate, self)._get_tax_code_value(template)
        if not code or code.valid_from != template.valid_from:
            res['valid_from'] = template.valid_from
        if not code or code.valid_to != template.valid_to:
            res['valid_to'] = template.valid_to
        if not code or code.description != template.description:
            res['description'] = template.description
        return res

CodeTemplate()


class Code(ModelSQL, ModelView):
    _name = 'account.tax.code'

    description = fields.Text('Description')

    def __init__(self):
        super(Code, self).__init__()
        self.parent = copy.copy(self.parent)
        if self.parent.domain is None:
            self.parent.domain = []
        domain_valid_from = ['OR',
                        ('valid_from', '<=', Eval('valid_from')),
                        ('valid_from', '=', False)
                        ]
        domain_valid_to = ['OR',
                        ('valid_to', '>=', Eval('valid_to')),
                        ('valid_to', '=', False)
                        ]
        pyson_domain_parent = PYSONEncoder().encode(self.parent.domain)
        if PYSONEncoder().encode(domain_valid_from) not in pyson_domain_parent:
            self.parent.domain += [domain_valid_from]
        if 'valid_from' not in self.parent.depends:
            self.parent.depends = copy.copy(self.parent.depends)
            self.parent.depends.append('valid_from')
        if PYSONEncoder().encode(domain_valid_to) not in pyson_domain_parent:
            self.parent.domain += [domain_valid_to]
        if 'valid_to' not in self.parent.depends:
            self.parent.depends = copy.copy(self.parent.depends)
            self.parent.depends.append('valid_to')

        self._reset_columns()

    valid_from = fields.Date('Valid from')
    valid_to = fields.Date('Valid to')

Code()


class Tax(ModelSQL, ModelView):
    _name = 'account.tax'

    accounts = fields.One2Many('tax.account', 'tax', 'Tax Accounts')
    codes = fields.One2Many('tax.code', 'tax', 'Tax Codes')
    search_key = fields.Char('Search Key', help='Can be used to define a '
        'custom key for the search.')

    def __init__(self):
        super(Tax, self).__init__()
        self.invoice_account = copy.copy(self.invoice_account)
        self.invoice_account = fields.Function(fields.Many2One(
                'account.account', 'Invoice Account'),
                'get_account', setter='set_account')

        self.credit_note_account = copy.copy(self.credit_note_account)
        self.credit_note_account = fields.Function(fields.Many2One(
                'account.account', 'Credit Note Account'),
                'get_account', setter='set_account')

        self.invoice_base_code = copy.copy(self.invoice_base_code)
        self.invoice_base_code = fields.Function(fields.Many2One(
                'account.tax.code', 'Invoice Base Code'),
                'get_code', setter='set_code')

        self.invoice_tax_code = copy.copy(self.invoice_tax_code)
        self.invoice_tax_code = fields.Function(fields.Many2One(
                'account.tax.code', 'Invoice Tax Code'),
                'get_code', setter='set_code')

        self.credit_note_base_code = copy.copy(self.credit_note_base_code)
        self.credit_note_base_code = fields.Function(fields.Many2One(
                'account.tax.code', 'Credit Note Base Code'),
                'get_code', setter='set_code')

        self.credit_note_tax_code = copy.copy(self.credit_note_tax_code)
        self.credit_note_tax_code = fields.Function(fields.Many2One(
                'account.tax.code', 'Credit Note Tax Code'),
                'get_code', setter='set_code')

        self.invoice_base_sign = copy.copy(self.invoice_base_sign)
        self.invoice_base_sign = fields.Function(fields.Numeric(
                'Invoice Base Sign'), 'get_sign', setter='set_sign')

        self.invoice_tax_sign = copy.copy(self.invoice_tax_sign)
        self.invoice_tax_sign = fields.Function(fields.Numeric(
                'Invoice Tax Sign'), 'get_sign', setter='set_sign')

        self.credit_note_base_sign = copy.copy(self.credit_note_base_sign)
        self.credit_note_base_sign = fields.Function(fields.Numeric(
                'Credit Note Base Sign'), 'get_sign', setter='set_sign')

        self.credit_note_tax_sign = copy.copy(self.credit_note_tax_sign)
        self.credit_note_tax_sign = fields.Function(fields.Numeric(
                'Credit Note Tax Sign'), 'get_sign', setter='set_sign')

        self._reset_columns()

    def init(self, module_name):
        super(Tax, self).init(module_name)
        table = TableHandler(Transaction().cursor, self, module_name)

        # Remove constraints on invoice_account and credit_note_account
        #as these fields cannot be used by this module
        table.not_null_action('invoice_account', action='remove')
        table.not_null_action('credit_note_account', action='remove')

    def search_rec_name(self, name, clause):
        ids = self.search(['OR',
            ('search_key',) + clause[1:],
            ('name',) + clause[1:]],
            order=[])
        if ids:
            return [('id', 'in', ids)]
        return [('name',) + clause[1:]]

    def get_account(self, ids, name):
        fiscalyear_obj = Pool().get('account.fiscalyear')
        tax_account_obj = Pool().get('tax.account')

        res = {}

        type = None
        if 'invoice_account' in name:
            type = 'invoice_account'
        if 'credit_note_account' in name:
            type = 'credit_note_account'

        fiscalyear = fiscalyear_obj.get_fiscalyear(
                date=Transaction().context.get('effective_date'))

        for id in ids:
            args = [
                ('type', '=', type),
                ('fiscalyear', '=', fiscalyear and fiscalyear.id or False),
                ('tax', '=', id),
                ]
            account_ids = tax_account_obj.search(args)
            if account_ids:
                account_rec = tax_account_obj.browse(account_ids[0])
                res[id] = account_rec.account.id
            else:
                res[id] = False
        return res

    def set_account(self, id, name, value):
        fiscalyear_obj = Pool().get('account.fiscalyear')
        tax_account_obj = Pool().get('tax.account')

        if not value:
            return True

        if Transaction().context.get('fiscalyear'):
            fiscalyear = fiscalyear_obj.browse(
                    Transaction().context['fiscalyear'])
        else:
            fiscalyear = fiscalyear_obj.get_fiscalyear(
                    date=Transaction().context.get('effective_date'))

        type = None
        if 'invoice_account' in name:
            type = 'invoice_account'
        if 'credit_note_account' in name:
            type = 'credit_note_account'

        vals = {
            'type': type,
            'fiscalyear': fiscalyear.id,
            'tax': id,
            'account': value,
            }

        args = [
            ('type', '=', type),
            ('fiscalyear', '=', fiscalyear.id),
            ('tax', '=', id),
            ]
        account_ids = tax_account_obj.search(args)
        if account_ids:
            account_id = account_ids[0]
            res = tax_account_obj.write(account_id, vals)
        else:
            res = tax_account_obj.create(vals)
        return res

    def get_code(self, ids, name):
        date_obj = Pool().get('ir.date')
        tax_code_obj = Pool().get('tax.code')

        res = {}

        type = None
        if 'invoice_base_code' in name:
            type = 'invoice_base'
        if 'invoice_tax_code' in name:
            type = 'invoice_tax'
        if 'credit_note_base_code' in name:
            type = 'credit_note_base'
        if 'credit_note_tax_code' in name:
            type = 'credit_note_tax'


        date = Transaction().context.get('effective_date', date_obj.today())
        for id in ids:
            args = [
                ('type', '=', type),
                ('tax', '=', id),
                ['OR',
                    ('valid_from', '<=', date),
                    ('valid_from', '=', False),
                    ],
                ['OR',
                    ('valid_to', '>=', date),
                    ('valid_to', '=', False),
                    ]
                ]
            code_ids = tax_code_obj.search(args)
            if code_ids:
                code_rec = tax_code_obj.browse(code_ids[0])
                res[id] = code_rec.code.id
            else:
                res[id] = False
        return res

    def set_code(self, id, name, value):
        date_obj = Pool().get('ir.date')
        tax_code_obj = Pool().get('tax.code')

        if not value:
            return True

        if isinstance(id, list):
            id = id[0]

        type = None
        if 'invoice_base_code' in name:
            type = 'invoice_base'
        if 'invoice_tax_code' in name:
            type = 'invoice_tax'
        if 'credit_note_base_code' in name:
            type = 'credit_note_base'
        if 'credit_note_tax_code' in name:
            type = 'credit_note_tax'

        date = Transaction().context.get('effective_date', date_obj.today())

        vals = {
            'type': type,
            'tax': id,
            'code': value,
            }

        args = [
            ('type', '=', type),
            ('tax', '=', id),
            ['OR',
                ('valid_from', '<=', date),
                ('valid_from', '=', False),
                ],
            ['OR',
                ('valid_to', '>=', date),
                ('valid_to', '=', False),
                ]
            ]
        code_ids = tax_code_obj.search(args)
        if code_ids:
            res = tax_code_obj.write(code_ids[0], vals)
        else:
            res = tax_code_obj.create(vals)
        return res

    def get_sign(self, ids, name):
        date_obj = Pool().get('ir.date')
        tax_code_obj = Pool().get('tax.code')

        res = {}

        type = None
        if 'invoice_base_sign' in name:
            type = 'invoice_base'
        if 'invoice_tax_sign' in name:
            type = 'invoice_tax'
        if 'credit_note_base_sign' in name:
            type = 'credit_note_base'
        if 'credit_note_tax_sign' in name:
            type = 'credit_note_tax'

        date = Transaction().context.get('effective_date', date_obj.today())
        for id in ids:
            args = [
                ('type', '=', type),
                ('tax', '=', id),
                ['OR',
                    ('valid_from', '<=', date),
                    ('valid_from', '=', False),
                    ],
                ['OR',
                    ('valid_to', '>=', date),
                    ('valid_to', '=', False),
                    ]
                ]
            sign_ids = tax_code_obj.search(args)
            if sign_ids:
                sign_rec = tax_code_obj.browse(sign_ids[0])
                res[id] = sign_rec.sign
            else:
                res[id] = Decimal('1')
        return res

    def set_sign(self, id, name, value):
        date_obj = Pool().get('ir.date')
        tax_code_obj = Pool().get('tax.code')

        if not value:
            return True

        if isinstance(id, list):
            id = id[0]

        type = None
        if 'invoice_base_sign' in name:
            type = 'invoice_base'
        if 'invoice_tax_sign' in name:
            type = 'invoice_tax'
        if 'credit_note_base_sign' in name:
            type = 'credit_note_base'
        if 'credit_note_tax_sign' in name:
            type = 'credit_note_tax'

        date = Transaction().context.get('effective_date', date_obj.today())

        vals = {
            'type': type,
            'tax': id,
            'sign': value,
            }

        args = [
            ('type', '=', type),
            ('tax', '=', id),
            ['OR',
                ('valid_from', '<=', date),
                ('valid_from', '=', False),
                ],
            ['OR',
                ('valid_to', '>=', date),
                ('valid_to', '=', False),
                ]
            ]
        sign_ids = tax_code_obj.search(args)
        if sign_ids:
            tax_code_obj.write(sign_ids[0], vals)
        #Attention New Tax Sign cannot be created. Only existing ones
        #can be written. The reason is that for new signs the corresponding
        #Tax Code is not available.
        return True

    def update_tax(self, tax, template2tax_code, template2account,
            template2tax=None):
        '''
        Update recursively taxes based on template.

        :param tax: a tax id or the BrowseRecord of the tax
        :param template2tax_code: a dictionary with tax code template id as key
                and tax code id as value, used to convert tax code template into
                tax code
        :param template2account: a dictionary with account template id as key
                and account id as value, used to convert account template into
                account code
        :param template2tax: a dictionary with tax template id as key and
                tax id as value, used to convert template id into tax.
                The dictionary is filled with new taxes
        '''
        template_obj = Pool().get('account.tax.template')
        lang_obj = Pool().get('ir.lang')

        if template2tax is None:
            template2tax = {}

        if isinstance(tax, (int, long)):
            tax = self.browse(tax)

        if tax.template:
            vals = template_obj._get_tax_value(tax.template, tax=tax)
            if vals.get('reverse_charge') and tax.childs:
                ids_tax_group = [tax.id]
                ids_tax_group.extend([x.id for x in tax.childs])
                self.write(ids_tax_group, {
                        'reverse_charge': vals['reverse_charge']
                        })
            if vals:
                self.write(tax.id, vals)

            prev_lang = tax._context.get('language') or 'en_US'
            for lang in lang_obj.get_translatable_languages():
                if lang == prev_lang:
                    continue
                tax.setLang(lang)
                with Transaction().set_context(language=lang):
                    data = template_obj._get_tax_value(tax.template, tax=tax)
                    if data:
                        self.write(tax.id, data)
            tax.setLang(prev_lang)
            template2tax[tax.template.id] = tax.id

        for child in tax.childs:
            self.update_tax(child, template2tax_code,
                    template2account, template2tax=template2tax)

Tax()


class TemplateCollection(ModelSQL, ModelView):
    'Template Collection'
    _name = 'account.tax.template.collection'
    _description = __doc__

    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)

TemplateCollection()


class TaxTemplate(ModelSQL, ModelView):
    _name = 'account.tax.template'

    template_collection = fields.Many2One(
            'account.tax.template.collection', 'Tax Template Collection',
            required=True)
    template_accounts = fields.One2Many('account.tax.tax.account.template',
            'tax', 'Accounts Templates')
    template_codes = fields.One2Many('account.tax.tax.code.template', 'tax',
            'Codes Templates')


    def __init__(self):
        super(TaxTemplate, self).__init__()
        self.account = copy.copy(self.account)
        self.account.required = False
        self._reset_columns()

    def init(self, module_name):
        super(TaxTemplate, self).init(module_name)
        table = TableHandler(Transaction().cursor, self, 'account')

        # account cannot be required as taxes are stored separately
        table.not_null_action('account', action='remove')

    def create_tax(self, template, company_id, template2tax_code,
            template2account, template2tax=None, parent_id=False):
        tax_obj = Pool().get('account.tax')

        new_id = super(TaxTemplate, self).create_tax(template, company_id,
                template2tax_code, template2account, template2tax=template2tax,
                parent_id=parent_id)

        vals = self._get_tax_value(template)

        tax_obj.write(new_id, vals)
        return new_id

TaxTemplate()


class RuleTemplate(ModelSQL, ModelView):
    _name = 'account.tax.rule.template'

    template_collection = fields.Many2One('account.tax.template.collection',
            'Tax Template Collection', required=True)

    def __init__(self):
        super(RuleTemplate, self).__init__()
        self.account = copy.copy(self.account)
        self.account.required = False
        self._reset_columns()

    def init(self, module_name):
        super(RuleTemplate, self).init(module_name)
        table = TableHandler(Transaction().cursor, self, 'account')

        # account cannot be required as Rules are stored separately
        table.not_null_action('account', action='remove')

RuleTemplate()


class Rule(ModelSQL, ModelView):
    _name = 'account.tax.rule'

    def apply(self, rule, tax, pattern):
        '''
        Apply rule on tax

        :param rule: a rule id or the BrowseRecord of the rule
        :param tax: a tax id or the BrowseRecord of the tax
        :param pattern: a dictonary with rule line field as key
                and match value as value
        :return: a list of the tax id to use or False
        '''
        tax_obj = Pool().get('account.tax')
        rule_line_obj = Pool().get('account.tax.rule.line')

        if isinstance(rule, (int, long)) and rule:
            rule = self.browse(rule)

        if isinstance(tax, (int, long)) and tax:
            tax = tax_obj.browse(tax)

        pattern = pattern.copy()
        if not pattern.get('group'):
            pattern['group'] = tax and tax.group.id or False
        if not pattern.get('origin_tax'):
            pattern['origin_tax'] = tax and tax.id or False

        for line in rule.lines:
            if rule_line_obj.match(line, pattern):
                return rule_line_obj.get_taxes(line)
        return tax and [tax.id] or False

Rule()


class RuleLineTemplate(ModelSQL, ModelView):
    _name = 'account.tax.rule.line.template'

    valid_from = fields.Date('Valid from')
    valid_to = fields.Date('Valid to')
    taxation_method = fields.Selection(_TAXATION_METHODS,
            #? required=True,
            'Taxation method', sort=False,
            help='The taxation method applicable for this rule line.')

    def __init__(self):
        super(RuleLineTemplate, self).__init__()
        self.tax = copy.copy(self.tax)
        domain_account = PYSONEncoder().encode(('account', '=',
                Get(Eval('_parent_rule', {}), 'account')))
        for domain in self.tax.domain:
            if domain_account == PYSONEncoder().encode(domain):
                self.tax.domain.remove(domain)
        domain_template_collection = ('template_collection', '=',
                Get(Eval('_parent_rule', {}), 'template_collection'))
        if PYSONEncoder().encode(domain_template_collection) not in \
            PYSONEncoder().encode(self.tax.domain):
            self.tax.domain += [domain_template_collection]
        self._reset_columns()

    def default_taxation_method(self):
        return 'accrual'

    def _get_tax_rule_line_value(self, template, rule_line=None):
        res = super(RuleLineTemplate, self)._get_tax_rule_line_value(template)
        if not rule_line or rule_line.valid_from != template.valid_from:
            res['valid_from'] = template.valid_from
        if not rule_line or rule_line.valid_to != template.valid_to:
            res['valid_from'] = template.valid_to
        if (not rule_line or rule_line.taxation_method !=
            template.taxation_method):
            res['taxation_method'] = template.taxation_method
        return res

RuleLineTemplate()


class RuleLine(ModelSQL, ModelView):
    _name = 'account.tax.rule.line'

    valid_from = fields.Date('Valid from')
    valid_to = fields.Date('Valid to')
    taxation_method = fields.Selection(_TAXATION_METHODS,
            #? required=True,
            'Taxation method', sort=False,
            help='The taxation method applicable for this rule line.')

    def default_taxation_method(self):
        return 'accrual'

    ##### BEGIN TODO: This should go upstream
    def match(self, line, pattern):
        '''
        Match line on pattern

        :param line: a BrowseRecord of rule line
        :param pattern: a dictonary with rule line field as key
                and match value as value
        :return: a Boolean
        '''
        res = True
        for field in pattern.keys():
            if not self.match_hook(line, pattern, field):
                res = False
                break
            if field == 'effective_date':
                if isinstance(pattern[field], datetime.datetime):
                    pattern[field] = pattern[field].date()
                if line['valid_from'] and line['valid_from'] > pattern[field]:
                    res = False
                    break
                if line['valid_to'] and line['valid_to'] < pattern[field]:
                    res = False
                    break
            if field not in self._columns:
                continue
            if not line[field] and field != 'group':
                continue
            if self._columns[field]._type == 'many2one':
                if line[field].id != pattern[field]:
                    res = False
                    break
            else:
                if line[field] != pattern[field]:
                    res = False
                    break
        return res

    def match_hook(self, line, pattern, field):
        '''
        Provide an extensible hook for pattern matching

        :param line: a BrowseRecord of rule line
        :param pattern: a dictonary with rule line field as key
                and match value as value
        :param field: a string of the field to check
        :return: a Boolean
        '''
        return True
    ##### END TODO: should go upstream

RuleLine()


class AccountTemplate(ModelSQL, ModelView):
    _name = 'account.account.template'

    template_collection = fields.Many2One(
            'account.tax.template.collection', 'Tax Template Collection')

AccountTemplate()


# TODO: Move to account_de module, when exist.
# Note: This requirement is not welcome upstream.
class Line(ModelSQL, ModelView):
    _name = 'account.tax.line'

    def fields_view_get(self, view_id=None, view_type='form', hexmd5=None):
        "This method sets all fields on account.tax.line to read only in views"
        result = super(Line, self).fields_view_get(view_id=view_id,
                view_type=view_type, hexmd5=None)
        for field in result['fields']:
            result['fields'][field]['readonly'] = True

        return result

Line()
