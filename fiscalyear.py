#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.pool import Pool

_TAXATION_METHODS = [
    ('accrual', 'Accrual Basis'),
    # TODO: move to cash_basis_submodule
    ('cash_income', 'Cash Basis (Income)'),
    ]


class FiscalYear(ModelSQL, ModelView):
    _name = 'account.fiscalyear'

    default_account_receivable = fields.Many2One('account.account',
            'Default Account Receivable',
            domain=[
                ('fiscalyear', '=', Eval('active_id')),
                ('company', '=', Eval('company')),
                ('kind', '=', 'receivable'),
                ],
            depends=['active_id', 'company'])
    default_account_payable = fields.Many2One('account.account',
            'Default Account Payable',
            domain=[
                ('fiscalyear', '=', Eval('active_id')),
                ('company', '=', Eval('company')),
                ('kind', '=', 'payable'),
                ],
            depends=['active_id', 'company'])
    all_successors = fields.Function(fields.One2Many(
            'account.fiscalyear', None, 'All Successors'),
            'get_all_successors', searcher='search_all_successors')
    # TODO: make readonly when moves exist
    taxation_method = fields.Selection(_TAXATION_METHODS,
            'Taxation method', sort=False, required=True,
            help='The taxation method used for this fiscal year.\n'
                'The default taxation method is Accrual Basis.')

    def default_taxation_method(self):
        # TODO: If previous year exists, take taxation_method default from ther
        return 'accrual'

    def __init__(self):
        super(FiscalYear, self).__init__()
        self._rpc.update({
            'get_fiscalyear_id': True,
        })

        self._error_messages.update({
            'no_successor': 'The account "%s" is set for deferral. ' \
                    'It must have a successor!',
            })

    def get_fiscalyear(self, date=None):
        res = self.get_fiscalyear_id(date=date)
        if res:
            res = self.browse(res)
        return res

    def get_fiscalyear_id(self, date=None):
        date_obj = Pool().get('ir.date')

        res = False

        if not date:
            date = date_obj.today()
        args = [
            ('start_date', '<=', date),
            ('end_date', '>=', date)
            ]
        fiscalyear_ids = self.search(args)
        if not fiscalyear_ids:
            #Get newest fiscalyear as fallback
            fiscalyear_ids = self.search([], limit=1,
                    order=[('end_date', 'DESC')])
        if fiscalyear_ids:
            res = fiscalyear_ids[0]
        return res

    def get_all_successors(self, ids, name):
        res = {}
        for fiscalyear in self.browse(ids):
            args = [('start_date', '>', fiscalyear.end_date)]
            fiscalyear_ids = self.search(args)
            if fiscalyear_ids:
                res[fiscalyear.id] = fiscalyear_ids
            else:
                res[fiscalyear.id] = False
        return res

    def search_all_successors(self, name, clause):
        res = []
        if clause[2]:
            for fiscalyear in self.browse(clause[2]):
                res.append(('start_date', '>', fiscalyear.end_date))
        return res

    def close(self, fiscalyear_id):
        '''
        Close a fiscal year

        :param fiscalyear_id: the fiscal year id
        '''
        period_obj = Pool().get('account.period')
        account_obj = Pool().get('account.account')

        if isinstance(fiscalyear_id, list):
            fiscalyear_id = fiscalyear_id[0]

        fiscalyear = self.browse(fiscalyear_id)

        if self.search([
            ('end_date', '<=', fiscalyear.start_date),
            ('state', '=', 'open'),
            ('company', '=', fiscalyear.company.id),
            ]):
            self.raise_user_error('close_error')

        #First close the fiscalyear to be sure
        #it will not have new period created between.
        self.write(fiscalyear_id, {
            'state': 'close',
            })
        period_ids = period_obj.search([
            ('fiscalyear', '=', fiscalyear_id),
            ])
        period_obj.close(period_ids)

        with Transaction().set_context(fiscalyear=fiscalyear_id, date=False):
            account_ids = account_obj.search([
                ('company', '=', fiscalyear.company.id),
                ('fiscalyear', '=', fiscalyear.id),
                ])
            accounts = account_obj.browse(account_ids)
        for account in accounts:
            self._process_account(account, fiscalyear)

    def _process_account(self, account, fiscalyear):
        '''
        Process account for a fiscal year closed

        :param account: a BrowseRecord of the account
        :param fiscalyear: a BrowseRecord of the fiscal year closed
        '''
        currency_obj = Pool().get('currency.currency')

        if account.close_condition == 'balanced':
            if not currency_obj.is_zero(fiscalyear.company.currency,
                    account.balance):
                self.raise_user_error('account_balance_not_zero',
                        error_args=(account.rec_name,))

FiscalYear()
