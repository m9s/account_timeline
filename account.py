#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
import datetime
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.report import Report
from trytond.tools import reduce_ids
from trytond.modules.account import PrintTrialBalance
from trytond.pyson import Eval, Not, Bool, If, Or, PYSONEncoder, Equal
from trytond.transaction import Transaction
from trytond.pool import Pool


class Type(ModelSQL, ModelView):
    _name = 'account.account.type'

    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            domain=[('company', '=', Eval('company'))
            ], depends=['company'], select=1, required=True)

    def __init__(self):
        super(Type, self).__init__()
        self._constraints += [
            ('check_unique_root_fiscalyear', 'one_root_per_fiscalyear'),
        ]
        self._error_messages.update({
            'one_root_per_fiscalyear': 'There can be only one root account type ' \
                    'per company and fiscalyear!',
            })

    def check_unique_root_fiscalyear(self, ids):
        res = True
        res2 = {}
        args = [('parent', '=', False)]
        root_ids = self.search(args)
        if root_ids:
            for root in self.browse(root_ids):
                if res2.has_key((root.company.id, root.fiscalyear.id)):
                    res = False
                else:
                    res2[(root.company.id, root.fiscalyear.id)] = 1
        return res

    def get_amount(self, ids, name):
        account_obj = Pool().get('account.account')
        currency_obj = Pool().get('currency.currency')

        res = {}
        for type_id in ids:
            res[type_id] = Decimal('0.0')

        child_ids = self.search([('parent', 'child_of', ids)])
        type_sum = {}
        for type_id in child_ids:
            type_sum[type_id] = Decimal('0.0')

        account_ids = account_obj.search([('type', 'in', child_ids)])
        for account in account_obj.browse(account_ids):
            type_sum[account.type.id] += currency_obj.round(
                    account.company.currency, account.starting_balance +
                    account.debit - account.credit)

        types = self.browse(ids)
        for type in types:
            child_ids = self.search([('parent', 'child_of', [type.id])])
            for child_id in child_ids:
                res[type.id] += type_sum[child_id]
            res[type.id] = currency_obj.round(type.company.currency,
                    res[type.id])
            if type.display_balance == 'credit-debit':
                res[type.id] = - res[type.id]
        return res

Type()


class Account(ModelSQL, ModelView):
    _name = 'account.account'

    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            domain=[('company', '=', Eval('company'))], select=1, required=True,
            states={
                'readonly': Bool(Eval('move_lines')),
                }, depends=['company', 'move_lines'])
    successor = fields.Many2One('account.account', 'Successor',
            domain=[
            ('fiscalyear', 'child_of', [Eval('fiscalyear')], 'all_successors')
            ],
            states={
                'invisible': Equal(Eval('kind'), 'view'),
                }, depends=['fiscalyear', 'kind'])
    predecessors = fields.One2Many('account.account', 'successor',
            'Predecessors', add_remove=[('company', '=', Eval('company'))],
            states={
                'invisible': Equal(Eval('kind'), 'view'),
                }, depends=['company', 'kind'])
    all_predecessors = fields.Function(fields.One2Many('account.account',
            None, 'All Predecessors'), 'get_all_predecessors')
    all_successors = fields.Function(fields.One2Many('account.account',
            None, 'All Successors'), 'get_all_successors')
    starting_balance = fields.Function(fields.Numeric('Starting Balance',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']), 'get_starting_balance')
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
            'get_currency_digits')
    move_lines = fields.One2Many('account.move.line', 'account', 'Move Lines',
            readonly=True, order=[('id', 'ASC')])
    close_condition = fields.Selection([
            ('balanced', 'Balanced'),
            ('none', 'None')
            ], 'Close Condition', required=True)
    opening_balance_account = fields.Boolean('Opening Balance Account',
            states={
                'readonly': Bool(Eval('move_lines')),
            }, depends=['move_lines'])

    def __init__(self):
        super(Account, self).__init__()
        self._rpc.update({'get_account_by_date': False})
        self._constraints += [
            ('check_unique_root_fiscalyear', 'one_root_per_fiscalyear'),
        ]
        self._error_messages.update({
            'one_root_per_fiscalyear': 'There can be only one root account ' \
                    'per company and fiscalyear!',
            })

        domain_fiscalyear = ('fiscalyear', '=', Eval('fiscalyear'))

        self.parent = copy.copy(self.parent)
        if self.parent.domain is None:
            self.parent.domain = []
        else:
            if PYSONEncoder().encode(domain_fiscalyear) not in \
                    PYSONEncoder().encode(self.parent.domain):
                self.parent.domain += [domain_fiscalyear]
        if 'fiscalyear' not in self.parent.depends:
            self.parent.depends = copy.copy(self.parent.depends)
            self.parent.depends.append('fiscalyear')

        self.type = copy.copy(self.type)
        if self.type.domain is None:
            self.type.domain = []
        else:
            if PYSONEncoder().encode(domain_fiscalyear) not in \
                    PYSONEncoder().encode(self.type.domain):
                self.type.domain += [domain_fiscalyear]
        if 'fiscalyear' not in self.type.depends:
            self.type.depends = copy.copy(self.type.depends)
            self.type.depends.append('fiscalyear')

        self._reset_columns()

    def default_close_condition(self):
        return 'none'

    def check_unique_root_fiscalyear(self, ids):
        res = True
        res2 = {}
        args = [('parent', '=', False)]
        root_ids = self.search(args)
        if root_ids:
            for root in self.browse(root_ids):
                if res2.has_key((root.company.id, root.fiscalyear.id)):
                    res = False
                else:
                    res2[(root.company.id, root.fiscalyear.id)] = 1
        return res

    def get_all_predecessors(self, ids, name):
        res = {}
        for account in self.browse(ids):
            res[account.id] = self._get_all_predecessors(account)
        return res

    def _get_all_predecessors(self, account):
        if isinstance(account, (int, long)):
            account = self.browse(account)
        res = []
        if account.predecessors:
            res.extend(x.id for x in account.predecessors)
            for predecessor in account.predecessors:
                res.extend(self._get_all_predecessors(predecessor))
        return res

    def get_all_successors(self, ids, name):
        res = {}
        for account in self.browse(ids):
            res[account.id] = self._get_all_successors(account)
        return res

    def _get_all_successors(self, account):
        if isinstance(account, (int, long)):
            account = self.browse(account)
        res = []
        if account.successor:
            res.append(account.successor.id)
            res.extend(self._get_all_successors(account.successor))
        return res

    def get_account_by_date(self, account, date=False):
        pool = Pool()
        date_obj = pool.get('ir.date')
        account_obj = pool.get('account.account')
        fiscalyear_obj = pool.get('account.fiscalyear')

        if not date:
            date = Transaction().context.get('effective_date') \
                    or date_obj.today()
        if isinstance(account, (int, long)):
            account = account_obj.browse(account)
        fiscalyear = fiscalyear_obj.get_fiscalyear(date)

        res = False
        if not account:
            return res

        all_account_ids = [account.id]
        all_account_ids += [x.id for x in account.all_predecessors]
        all_account_ids += [x.id for x in account.all_successors]

        args = [
            ('fiscalyear', '=', fiscalyear.id),
            ('id', 'in', all_account_ids)
            ]
        account_ids = account_obj.search(args)
        if account_ids:
            res = account_ids[0]
        return res

    def get_currency_digits(self, ids, name):
        res = {}
        for account in self.browse(ids):
            res[account.id] = account.company.currency.digits
        return res

    def get_starting_balance(self, ids, name):
        res = {}
        cursor = Transaction().cursor
        cursor.execute('SELECT a.id, '
                'SUM((COALESCE(l.debit, 0) - COALESCE(l.credit, 0))) '
            'FROM account_account a '
                'LEFT JOIN account_move_line l '
                'ON (a.id = l.account) '
            'WHERE a.id IN (%s) '
                'AND l.move IN ('
                    'SELECT id FROM account_move m '
                    'WHERE m.journal IN ('
                        'SELECT id FROM account_journal j '
                        "WHERE j.type = 'situation')) "
            'GROUP BY a.id' % (','.join(map(str, ids)),))

        for account_id, sum in cursor.fetchall():
            # SQLite uses float for SUM
            if not isinstance(sum, Decimal):
                sum = Decimal(str(sum))
            res[account_id] = sum

        for account in self.browse(ids):
            res.setdefault(account.id, Decimal('0.0'))
        return res

    def get_balance(self, ids, name):
        res = {}
        pool = Pool()
        currency_obj = pool.get('currency.currency')
        move_line_obj = pool.get('account.move.line')

        if not ids:
            return {}

        round = True
        if Transaction().context.get('no_rounding'):
            round = False

        query_ids, args_ids = self.search([
            ('parent', 'child_of', ids),
            ], query_string=True)
        line_query, fiscalyear_ids = move_line_obj.query_get()
        cursor = Transaction().cursor
        cursor.execute('SELECT a.id, ' \
                    'SUM((COALESCE(l.debit, 0) - COALESCE(l.credit, 0))) ' \
                'FROM account_account a ' \
                    'LEFT JOIN account_move_line l ' \
                    'ON (a.id = l.account) ' \
                'WHERE a.kind != \'view\' ' \
                    'AND a.id IN (' + query_ids + ') ' \
                    'AND ' + line_query + ' ' \
                    'AND a.active ' \
                'GROUP BY a.id', args_ids)
        account_sum = {}
        for account_id, sum in cursor.fetchall():
            # SQLite uses float for SUM
            if not isinstance(sum, Decimal):
                sum = Decimal(str(sum))
            account_sum[account_id] = sum

        account2company = {}
        id2company = {}
        id2account = {}
        all_ids = self.search([('parent', 'child_of', ids)])

        accounts = self.browse(all_ids)
        for account in accounts:
            account2company[account.id] = account.company.id
            id2company[account.company.id] = account.company
            id2account[account.id] = account

        for account_id in ids:
            res.setdefault(account_id, Decimal('0.0'))
            child_ids = self.search([('parent', 'child_of', [account_id])])
            company_id = account2company[account_id]
            to_currency = id2company[company_id].currency
            for child in self.browse(child_ids):
                child_company_id = account2company[child.id]
                from_currency = id2company[child_company_id].currency
                res[account_id] += currency_obj.compute(from_currency,
                        account_sum.get(child.id, Decimal('0.0')),
                        to_currency, round=round)

        if round:
            for account_id in ids:
                company_id = account2company[account_id]
                to_currency = id2company[company_id].currency
                res[account_id] = currency_obj.round(to_currency,
                        res[account_id])
        return res

    def get_credit_debit(self, ids, names, include_situation=False):
        '''
        Function to compute debit, credit for account ids.

        :param ids: the ids of the account
        :param names: the list of field name to compute
        :return: a dictionary with all field names as key and
            a dictionary as value with id as key
        '''
        pool = Pool()
        move_line_obj = pool.get('account.move.line')
        currency_obj = pool.get('currency.currency')
        cursor = Transaction().cursor

        res = {}
        for name in names:
            if name not in ('credit', 'debit'):
                raise Exception('Bad argument')
            res[name] = {}

        line_query, fiscalyear_ids = move_line_obj.query_get()
        situation_query = 'AND l.move NOT IN (' \
            'SELECT id FROM account_move m ' \
            'WHERE m.journal IN (' \
                'SELECT id FROM account_journal j ' \
                "WHERE j.type = 'situation'))"
        if include_situation:
            situation_query = ''
        for i in range(0, len(ids), cursor.IN_MAX):
            sub_ids = ids[i:i + cursor.IN_MAX]
            red_sql, red_ids = reduce_ids('a.id', sub_ids)
            cursor.execute('SELECT a.id, ' +
                        ','.join('SUM(COALESCE(l.' + name + ', 0))' \
                            for name in names) + ' '
                    'FROM account_account a '
                        'LEFT JOIN account_move_line l '
                        'ON (a.id = l.account) '
                    'WHERE a.kind != \'view\' '
                        'AND ' + red_sql + ' '
                        'AND ' + line_query + ' '
                        'AND a.active '
                        + situation_query + ' '
                    'GROUP BY a.id', red_ids)
            for row in cursor.fetchall():
                account_id = row[0]
                for i in range(len(names)):
                    # SQLite uses float for SUM
                    if not isinstance(row[i + 1], Decimal):
                        res[names[i]][account_id] = Decimal(str(row[i + 1]))
                    else:
                        res[names[i]][account_id] = row[i + 1]

        account2company = {}
        id2company = {}
        accounts = self.browse(ids)
        for account in accounts:
            account2company[account.id] = account.company.id
            id2company[account.company.id] = account.company

        for account_id in ids:
            for name in names:
                res[name].setdefault(account_id, Decimal('0.0'))

        for account_id in ids:
            company_id = account2company[account_id]
            currency = id2company[company_id].currency
            for name in names:
                res[name][account_id] = currency_obj.round(currency,
                        res[name][account_id])
        return res

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['move_lines'] = False
        return super(Account, self).copy(ids, default=default)

    def update_account(self, account, template2account=None,
            template2type=None, previoustemplate2account=None):
        '''
        Update recursively accounts based on template.

        :param account: an account id or the BrowseRecord of the account
        :param template2account: a dictionary with template id as key
                and account id as value, used to convert template id
                into account. The dictionary is filled with new accounts
        :param template2type: a dictionary with type template id as key
                and type id as value, used to convert type template id
                into type.
        '''
        template_obj = Pool().get('account.account.template')
        lang_obj = Pool().get('ir.lang')

        if template2account is None:
            template2account = {}

        if template2type is None:
            template2type = {}

        if previoustemplate2account is None:
            previoustemplate2account = {}

        if isinstance(account, (int, long)):
            account = self.browse(account)

        if account.template:
            vals = template_obj._get_account_value(account.template,
                    account=account)
            if account.type.id != template2type.get(account.template.type.id,
                    False):
                vals['type'] = template2type.get(account.template.type.id,
                        False)
            predecessor_ids = []
            for predecessor in account.template.predecessors:
                if not previoustemplate2account.get(predecessor.id):
                    continue
                predecessor_ids += [previoustemplate2account[predecessor.id]]
            if predecessor_ids:
                vals['predecessors'] = [('set', predecessor_ids)]
            if vals:
                self.write(account.id, vals)

            prev_lang = account._context.get('language') or 'en_US'
            for lang in lang_obj.get_translatable_languages():
                if lang == prev_lang:
                    continue
                account.setLang(lang)
                with Transaction().set_context(language=lang):
                    data = template_obj._get_account_value(account.template,
                            account=account)
                    if data:
                        self.write(account.id, data)
            account.setLang(prev_lang)
            template2account[account.template.id] = account.id

        for child in account.childs:
            self.update_account(child, template2account=template2account,
                    template2type=template2type,
                    previoustemplate2account=previoustemplate2account)

Account()


class CreateChartAccountAccountFiscalyear(ModelView):
    'Create Chart Account Account Fiscalyear'
    _name = 'account.account.create_chart_account.account.fiscalyear'
    _description = __doc__
    company = fields.Many2One('company.company', 'Company', required=True)
    account_template = fields.Many2One('account.account.template',
            'Account Template', required=True, domain=[('parent', '=', False)])
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True)

CreateChartAccountAccountFiscalyear()


class CreateChartAccountPropertitesFiscalyear(ModelView):
    'Create Chart Account Properties Fiscalyear'
    _name = 'account.account.create_chart_account.properties.fiscalyear'
    _description = __doc__
    company = fields.Many2One('company.company', 'Company')
    account_receivable = fields.Many2One('account.account',
            'Default Receivable Account',
            domain=[
                ('kind', '=', 'receivable'),
                ('company', '=', Eval('company')),
                ('fiscalyear', '=', Eval('fiscalyear'))
                ],
            depends=['company', 'fiscalyear'])
    account_payable = fields.Many2One('account.account',
            'Default Payable Account',
            domain=[
                    ('kind', '=', 'payable'),
                    ('company', '=', Eval('company')),
                    ('fiscalyear', '=', Eval('fiscalyear'))
                    ],
            depends=['company', 'fiscalyear'])
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True, readonly=True)

CreateChartAccountPropertitesFiscalyear()


class CreateChartAccountFiscalyear(Wizard):
    'Create Chart Account Fiscalyear'
    _name = 'account.account.create_chart_account.fiscalyear'
    _description = __doc__

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'account.account.create_chart_account.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('account', 'Ok', 'tryton-ok', True),
                ],
            },
        },
        'account': {
            'result': {
                'type': 'form',
                'object': 'account.account.create_chart_account.account.fiscalyear',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('create_account', 'Create', 'tryton-ok', True),
                ],
            },
        },
        'create_account': {
            'actions': ['_action_create_account'],
            'result': {
                'type': 'form',
                'object': 'account.account.create_chart_account.properties.fiscalyear',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('create_properties', 'Create', 'tryton-ok', True),
                ],
            },
        },
        'create_properties': {
            'result': {
                'type': 'action',
                'action': '_action_create_properties',
                'state': 'end',
            },
        },
    }

    def _action_create_account(self, datas):
        pool = Pool()
        account_type_template_obj = pool.get('account.account.type.template')
        account_template_obj = pool.get('account.account.template')
        tax_code_template_obj = pool.get('account.tax.code.template')
        tax_account_template_obj = pool.get('account.tax.tax.account.template')
        tax_tax_code_template_obj = pool.get('account.tax.tax.code.template')
        tax_template_obj = pool.get('account.tax.template')
        tax_rule_template_obj = pool.get('account.tax.rule.template')
        tax_rule_line_template_obj = pool.get('account.tax.rule.line.template')
        tax_obj = pool.get('account.tax')
        tax_rule_obj = pool.get('account.tax.rule')
        tax_rule_line_obj = pool.get('account.tax.rule.line')
        fiscalyear_obj = pool.get('account.fiscalyear')
        account_obj = pool.get('account.account')

        context = {}
        context['language'] = 'en_US'
        context['fiscalyear'] = datas['form']['fiscalyear']
        context['no_update_tree'] = True
        with Transaction().set_context(**context):
            account_template = account_template_obj.browse(
                    datas['form']['account_template'])

            # Create account types
            template2type = {}
            account_type_template_obj.create_type(account_template.type,
                    datas['form']['company'], template2type=template2type)

            previoustemplate2account = {}
            if context.get('fiscalyear'):
                fiscalyear = fiscalyear_obj.browse(context['fiscalyear'])
                args = [
                    ('end_date', '<', fiscalyear.start_date),
                    ('company', '=', datas['form']['company'])
                    ]
                previous_year_id = fiscalyear_obj.search(args, limit=1,
                        order=[('end_date', 'DESC')])

                if previous_year_id:
                    args = [
                        ('fiscalyear', '=', previous_year_id[0]),
                        ('company', '=', datas['form']['company'])
                        ]
                    previous_account_ids = account_obj.search(args)
                    if previous_account_ids:
                        for previous_account in account_obj.browse(
                                previous_account_ids):
                            if previous_account.template:
                                previoustemplate2account[
                                        previous_account.template.id] = \
                                        previous_account.id

            # Create accounts
            template2account = {}
            account_template_obj.create_account(
                    account_template, datas['form']['company'],
                    template2account=template2account,
                    template2type=template2type,
                    previoustemplate2account=previoustemplate2account)

            # Update MPTT of accounts once for all
            account_obj._rebuild_tree('parent', False, 0)

            # Create tax codes
            template2tax_code = {}
            tax_code_template_ids = tax_code_template_obj.search([
                ('account', '=', datas['form']['account_template']),
                ('parent', '=', False),
                ])
            for tax_code_template in tax_code_template_obj.browse(
                    tax_code_template_ids):
                tax_code_template_obj.create_tax_code(tax_code_template,
                        datas['form']['company'],
                        template2tax_code=template2tax_code)

            # Update taxes
            template2tax = {}
            tax_ids = tax_obj.search([
                ('company', '=', datas['form']['company']),
                ('parent', '=', False),
                ])
            if tax_ids:
                for tax in tax_obj.browse(tax_ids):
                    tax_obj.update_tax(tax,
                            template2tax_code=template2tax_code,
                            template2account=template2account,
                            template2tax=template2tax)

            # Create missing taxes
            tax_template_ids = tax_template_obj.search([
                ('template_collection', '=',
                        account_template.template_collection.id),
                ('parent', '=', False),
                ])
            for tax_template in tax_template_obj.browse(tax_template_ids):
                tax_template_obj.create_tax(tax_template,
                        datas['form']['company'],
                        template2tax_code=template2tax_code,
                        template2account=template2account,
                        template2tax=template2tax)

            # Update taxes on accounts
            account_template_obj.update_account_taxes(account_template,
                    template2account, template2tax)

            # Create tax account entries
            template2tax_account = {}
            tax_account_template_ids = tax_account_template_obj.search([
                ('account_template', '=', datas['form']['account_template']),
                ])
            for tax_account_template in tax_account_template_obj.browse(
                    tax_account_template_ids):
                tax_account_template_obj.create_tax_account(
                        tax_account_template, datas['form']['fiscalyear'],
                        template2account=template2account,
                        template2tax=template2tax,
                        template2tax_account=template2tax_account)

            # Create tax code entries
            template2tax_tax_code = {}
            tax_tax_code_template_ids = tax_tax_code_template_obj.search([
                    ('account_template', '=', datas['form']['account_template']),
                ])
            for tax_tax_code_template in tax_tax_code_template_obj.browse(
                    tax_tax_code_template_ids):
                tax_tax_code_template_obj.create_tax_tax_code(
                        tax_tax_code_template,
                        template2tax_code=template2tax_code,
                        template2tax=template2tax,
                        template2tax_tax_code=template2tax_tax_code)

            # Update tax rules
            template2rule = {}
            tax_rule_ids = tax_rule_obj.search([
                ('company', '=', datas['form']['company']),
                ])
            if tax_rule_ids:
                for tax_rule in tax_rule_obj.browse(tax_rule_ids):
                    tax_rule_obj.update_rule(tax_rule,
                            template2rule=template2rule)
            # Create missing tax rules
            tax_rule_template_ids = tax_rule_template_obj.search([
                ('template_collection', '=',
                        account_template.template_collection.id),
                ])
            for tax_rule_template in tax_rule_template_obj.browse(
                    tax_rule_template_ids):
                tax_rule_template_obj.create_rule(tax_rule_template,
                        datas['form']['company'], template2rule=template2rule)

            # Update tax rule lines
            template2rule_line = {}
            tax_rule_line_ids = tax_rule_line_obj.search([
                ('rule.company', '=', datas['form']['company']),
                ])
            if tax_rule_line_ids:
                for tax_rule_line in tax_rule_line_obj.browse(tax_rule_line_ids):
                    tax_rule_line_obj.update_rule_line(tax_rule_line,
                            template2tax, template2rule,
                            template2rule_line=template2rule_line)
            # Create missing tax rule lines
            tax_rule_line_template_ids = tax_rule_line_template_obj.search([
                        ('rule.template_collection', '=',
                                     account_template.template_collection.id),
                        ])
            for tax_rule_line_template in tax_rule_line_template_obj.browse(
                    tax_rule_line_template_ids):
                tax_rule_line_template_obj.create_rule_line(
                        tax_rule_line_template, template2tax, template2rule,
                        template2rule_line=template2rule_line)

        return {'company': datas['form']['company'],
                'fiscalyear': datas['form']['fiscalyear']}

    def _action_create_properties(self, datas):
        fiscalyear_obj = Pool().get('account.fiscalyear')
        party_obj = Pool().get('party.party')

        vals = {
            'default_account_receivable': datas['form']['account_receivable'],
            'default_account_payable': datas['form']['account_payable']
            }
        fiscalyear_obj.write(datas['form']['fiscalyear'], vals)

        party_ids = party_obj.search([])
        parties = party_obj.browse(party_ids)

        new_accounts = [
            {
                'create_date': datetime.datetime.now(),
                'create_uid': Transaction().user,
                'fiscalyear': datas['form']['fiscalyear'],
                'account': datas['form']['account_receivable'],
                'type': 'receivable',
            },{
                'create_date': datetime.datetime.now(),
                'create_uid': Transaction().user,
                'fiscalyear': datas['form']['fiscalyear'],
                'account': datas['form']['account_payable'],
                'type': 'payable'
            }]

        account_data = []
        for values in new_accounts:
            for party in parties:
                vals = values.copy()
                vals.update({'party': party.id})
                account_data.append(vals)

        if account_data:
            fields2 = [x for x in account_data[0].keys()]
            item_fields = ",".join(x for x in fields2)
            item_fields2 = ",".join('%(' + x + ')s' for x in fields2)
            Transaction().cursor.executemany('INSERT INTO party_account(' + \
                    item_fields + ') VALUES (' + item_fields2 + ')',
                    account_data)

        return {}

CreateChartAccountFiscalyear()


class UpdateChartAccountFiscalyear(Wizard):
    'Update Chart Account from Template'
    _name = 'account.account.update_chart_account.fiscalyear'
    _description = __doc__

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'account.account.update_chart_account.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('start', 'Ok', 'tryton-ok', True),
                ],
            },
        },
        'start': {
            'actions': ['_action_update_account'],
            'result': {
                'type': 'form',
                'object': 'account.account.update_chart_account.start',
                'state': [
                    ('end', 'Ok', 'tryton-ok', True),
                ],
            },
        },
    }

    def _action_update_account(self, datas):
        pool = Pool()
        account_type_obj = pool.get('account.account.type')
        account_type_template_obj = pool.get('account.account.type.template')
        account_obj = pool.get('account.account')
        account_template_obj = pool.get('account.account.template')
        tax_code_obj = pool.get('account.tax.code')
        tax_code_template_obj = pool.get('account.tax.code.template')
        tax_obj = pool.get('account.tax')
        tax_template_obj = pool.get('account.tax.template')
        tax_rule_obj = pool.get('account.tax.rule')
        tax_rule_template_obj = pool.get('account.tax.rule.template')
        tax_rule_line_obj = pool.get('account.tax.rule.line')
        tax_rule_line_template_obj = pool.get('account.tax.rule.line.template')
        tax_account_obj = pool.get('tax.account')
        tax_account_template_obj = pool.get('account.tax.tax.account.template')
        tax_tax_code_obj = pool.get('tax.code')
        tax_tax_code_template_obj = pool.get('account.tax.tax.code.template')
        fiscalyear_obj = pool.get('account.fiscalyear')

        account = account_obj.browse(datas['form']['account'])

        context = {}
        context['fiscalyear'] = account.fiscalyear.id,
        context['no_update_tree'] = True
        with Transaction().set_context(**context):
            # Update account types
            template2type = {}
            account_type_obj.update_type(account.type,
                    template2type=template2type)
            # Create missing account types
            if account.type.template:
                account_type_template_obj.create_type(account.type.template,
                        account.company.id, template2type=template2type)

            previoustemplate2account = {}
            args = [
                ('end_date', '<', account.fiscalyear.start_date),
                ('company', '=', account.company.id)
                ]
            previous_year_id = fiscalyear_obj.search(args, limit=1,
                    order=[('end_date', 'DESC')])

            if previous_year_id:
                args = [
                    ('fiscalyear', '=', previous_year_id[0]),
                    ('company', '=', account.company.id)
                    ]
                previous_account_ids = account_obj.search(args)
                if previous_account_ids:
                    for previous_account in account_obj.browse(
                            previous_account_ids):
                        if previous_account.template:
                            previoustemplate2account[
                                    previous_account.template.id] = \
                                    previous_account.id

            # Update accounts
            template2account = {}
            account_obj.update_account(account,
                    template2account=template2account,
                    template2type=template2type,
                    previoustemplate2account=previoustemplate2account)

            # Update MPTT of accounts once for all
            account_obj._rebuild_tree('parent', False, 0)

            # Create missing accounts
            if account.template:
                account_template_obj.create_account(account.template,
                        account.company.id, template2account=template2account,
                        template2type=template2type,
                        previoustemplate2account=previoustemplate2account)

            # Update tax codes
            template2tax_code = {}
            tax_code_ids = tax_code_obj.search([
                ('company', '=', account.company.id),
                ('parent', '=', False),
                ])
            for tax_code in tax_code_obj.browse(tax_code_ids):
                tax_code_obj.update_tax_code(tax_code,
                        template2tax_code=template2tax_code)
            # Create missing tax codes
            if account.template:
                tax_code_template_ids = tax_code_template_obj.search([
                    ('account', '=', account.template.id),
                    ('parent', '=', False),
                    ])
                for tax_code_template in tax_code_template_obj.browse(
                        tax_code_template_ids):
                    tax_code_template_obj.create_tax_code(tax_code_template,
                            account.company.id,
                            template2tax_code=template2tax_code)

            # Update taxes
            template2tax = {}
            tax_ids = tax_obj.search([
                ('company', '=', account.company.id),
                ('parent', '=', False),
                ])
            for tax in tax_obj.browse(tax_ids):
                tax_obj.update_tax(tax, template2tax_code=template2tax_code,
                        template2account=template2account,
                        template2tax=template2tax)
            # Create missing taxes
            if account.template:
                tax_template_ids = tax_template_obj.search([
                    ('template_collection', '=',
                        account.template.template_collection),
                    ('parent', '=', False),
                    ])
                for tax_template in tax_template_obj.browse(tax_template_ids):
                    tax_template_obj.create_tax(tax_template,
                            account.company.id,
                            template2tax_code=template2tax_code,
                            template2account=template2account,
                            template2tax=template2tax)

            # Update taxes on accounts
            account_obj.update_account_taxes(account, template2account,
                    template2tax)

            # Update tax account entries
            template2tax_account = {}
            tax_account_ids = tax_account_obj.search([
                ('tax.company', '=', account.company.id),
                ('fiscalyear', '=', account.fiscalyear.id)
                ])
            for tax_account in tax_account_obj.browse(tax_account_ids):
                tax_account_obj.update_tax_account(tax_account,
                        account.fiscalyear.id,
                        template2account=template2account,
                        template2tax=template2tax,
                        template2tax_account=template2tax_account)

            # Create missing tax account entries
            tax_account_template_ids = tax_account_template_obj.search([
                ('account_template', '=', account.template.id),
                ])
            for tax_account_template in tax_account_template_obj.browse(
                    tax_account_template_ids):
                tax_account_template_obj.create_tax_account(tax_account_template,
                        account.fiscalyear.id, template2account=template2account,
                        template2tax=template2tax,
                        template2tax_account=template2tax_account)

            # Update tax tax code entries
            template2tax_tax_code = {}
            tax_tax_code_ids = tax_tax_code_obj.search([
                ('tax.company', '=', account.company.id),
                ['OR', ('valid_from', '<', account.fiscalyear.end_date),
                ('valid_from', '=', False)],
                ['OR', ('valid_to', '>', account.fiscalyear.start_date),
                ('valid_to', '=', False)],
                ])
            for tax_tax_code in tax_tax_code_obj.browse(tax_tax_code_ids):
                tax_tax_code_obj.update_tax_tax_code(tax_tax_code,
                        template2tax_code=template2tax_code,
                        template2tax=template2tax,
                        template2tax_tax_code=template2tax_tax_code)

            # Create missing tax tax code entries
            tax_tax_code_template_ids = tax_tax_code_template_obj.search([
                ('account_template', '=', account.template.id),
                ])
            for tax_tax_code_template in tax_tax_code_template_obj.browse(
                    tax_tax_code_template_ids):
                tax_tax_code_template_obj.create_tax_tax_code(
                        tax_tax_code_template,
                        template2tax_code=template2tax_code,
                        template2tax=template2tax,
                        template2tax_tax_code=template2tax_tax_code)

            # Update tax rules
            template2rule = {}
            tax_rule_ids = tax_rule_obj.search([
                ('company', '=', account.company.id),
                ])
            for tax_rule in tax_rule_obj.browse(tax_rule_ids):
                tax_rule_obj.update_rule(tax_rule, template2rule=template2rule)
            # Create missing tax rules
            if account.template:
                tax_rule_template_ids = tax_rule_template_obj.search([
                    ('template_collection', '=',
                             account.template.template_collection.id),
                    ])
                for tax_rule_template in tax_rule_template_obj.browse(
                        tax_rule_template_ids):
                    tax_rule_template_obj.create_rule(tax_rule_template,
                            account.company.id, template2rule=template2rule)

            # Update tax rule lines
            template2rule_line = {}
            tax_rule_line_ids = tax_rule_line_obj.search([
                ('rule.company', '=', account.company.id),
                ])
            for tax_rule_line in tax_rule_line_obj.browse(tax_rule_line_ids):
                tax_rule_line_obj.update_rule_line(tax_rule_line, template2tax,
                        template2rule, template2rule_line=template2rule_line)
            # Create missing tax rule lines
            if account.template:
                tax_rule_line_template_ids = tax_rule_line_template_obj.search([
                            ('rule.template_collection', '=',
                                     account.template.template_collection.id),
                            ])
                for tax_rule_line_template in tax_rule_line_template_obj.browse(
                        tax_rule_line_template_ids):
                    tax_rule_line_template_obj.create_rule_line(
                            tax_rule_line_template, template2tax, template2rule,
                            template2rule_line=template2rule_line)
        return {}

UpdateChartAccountFiscalyear()



class TypeTemplate(ModelSQL, ModelView):
    _name = 'account.account.type.template'

    def _get_type_value(self, template, type=None):
        res = super(TypeTemplate, self)._get_type_value(template, type=type)

        res['fiscalyear'] = Transaction().context.get('fiscalyear')
        return res

TypeTemplate()


class AccountTemplate(ModelSQL, ModelView):
    _name = 'account.account.template'

    successor = fields.Many2One('account.account.template', 'Successor')
    predecessors = fields.One2Many('account.account.template', 'successor',
            'Predecessors')
    close_condition = fields.Selection([
            ('balanced', 'Balanced'),
            ('none', 'None')
            ], 'Close Condition', required=True)
    opening_balance_account = fields.Boolean('Opening Balance Account')

    def default_close_condition(self):
        return 'none'

    def create_account(self, template, company_id, template2account=None,
            template2type=None, parent_id=False,
            previoustemplate2account=None):
        '''
        Create recursively accounts based on template.

        :param template: the template id or the BrowseRecord of template
                used for account creation
        :param company_id: the id of the company for which accounts
                are created
        :param template2account: a dictionary with template id as key
                and account id as value, used to convert template id
                into account. The dictionary is filled with new accounts
        :param template2type: a dictionary with type template id as key
                and type id as value, used to convert type template id
                into type.
        :param parent_id: the account id of the parent of the accounts
                that must be created
        :return: id of the account created
        '''
        pool = Pool()
        account_obj = pool.get('account.account')
        lang_obj = pool.get('ir.lang')

        if template2account is None:
            template2account = {}

        if template2type is None:
            template2type = {}

        if previoustemplate2account is None:
            previoustemplate2account = {}

        if isinstance(template, (int, long)):
            template = self.browse(template)

        if template.id not in template2account:
            vals = self._get_account_value(template)
            vals['company'] = company_id
            vals['parent'] = parent_id
            vals['type'] = template2type.get(template.type.id, False)

            predecessor_ids = []
            for predecessor in template.predecessors:
                if not previoustemplate2account.get(predecessor.id):
                    continue
                predecessor_ids += [previoustemplate2account[predecessor.id]]
            if predecessor_ids:
                vals['predecessors'] = [('set', predecessor_ids)]

            new_id = account_obj.create(vals)

            prev_lang = template._context.get('language') or 'en_US'
            for lang in lang_obj.get_translatable_languages():
                if lang == prev_lang:
                    continue
                template.setLang(lang)
                data = {}
                for field in template._columns.keys():
                    if getattr(template._columns[field], 'translate', False):
                        data[field] = template[field]
                if data:
                    with Transaction().set_context(language=lang):
                        account_obj.write(new_id, data)
            template.setLang(prev_lang)
            template2account[template.id] = new_id
        else:
            new_id = template2account[template.id]

        new_childs = []
        for child in template.childs:
            new_childs.append(self.create_account(child, company_id,
                template2account=template2account,
                template2type=template2type, parent_id=new_id,
                previoustemplate2account=previoustemplate2account))
        return new_id

    def _get_account_value(self, template, account=None):
        res = super(AccountTemplate, self)._get_account_value(template,
                account=account)

        res['fiscalyear'] = Transaction().context.get('fiscalyear')
        if not account or account.close_condition != template.close_condition:
            res['close_condition'] = template.close_condition
        if not account or account.opening_balance_account != \
                template.opening_balance_account:
            res['opening_balance_account'] = template.opening_balance_account
        return res

AccountTemplate()


class OpenBalanceSheet(Wizard):
    _name = 'account.account.open_balance_sheet'

    def _action_open(self, datas):
        fiscalyear_obj = Pool().get('account.fiscalyear')
        res = super(OpenBalanceSheet, self)._action_open(datas)
        date = datas['form']['date']
        args = [
            ('start_date', '<=', date),
            ('end_date', '>=', date)
            ]
        fiscalyear_ids = fiscalyear_obj.search(args)
        if fiscalyear_ids:
            ctx = {}
            ctx['fiscalyear'] = fiscalyear_ids[0]
            pyson_ctx = PYSONEncoder().encode(ctx)
            res['pyson_context'] = res['pyson_context'][:-1] + ", " + \
                    pyson_ctx[1:-1] + "}"
        return res

OpenBalanceSheet()


class OpenIncomeStatement(Wizard):
    _name = 'account.account.open_income_statement'

    def _action_open(self, datas):
        res = super(OpenIncomeStatement, self)._action_open(datas)
        ctx = {}
        ctx['fiscalyear'] = datas['form']['fiscalyear']
        pyson_ctx = PYSONEncoder().encode(ctx)
        res['pyson_context'] = res['pyson_context'][:-1] + ", " + \
                pyson_ctx[1:-1] + "}"
        return res

OpenIncomeStatement()


class PrintGeneralLegderFiscalyear(Wizard):
    'Print General Legder'
    _name = 'account.account.print_general_ledger.fiscalyear'
    _description = __doc__

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'account.account.print_general_ledger.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('print', 'Print', 'tryton-print', True),
                ],
            },
        },
        'print': {
            'result': {
                'type': 'print',
                'report': 'account.account.general_ledger.fiscalyear',
                'state': 'end',
            },
        },
    }

PrintGeneralLegderFiscalyear()


class PrintGeneralLedgerInit(ModelView):
    _name = 'account.account.print_general_ledger.init'

    range_accounts = fields.Selection([
        ('all', 'All Accounts'),
        ('selected', 'Selected Accounts'),
        ], 'Account Range', required=True)
    accounts = fields.Many2Many('account.account', None, None, 'Accounts',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('kind', '!=', 'view')
        ], states={'invisible': Not(Equal(Eval('range_accounts'), 'selected'))},
        depends=['range_accounts', 'fiscalyear'])

    def default_range_accounts(self):
        return 'all'

PrintGeneralLedgerInit()


class GeneralLegderFiscalyear(Report):
    'General Ledger Fiscalyear'
    _name = 'account.account.general_ledger.fiscalyear'
    _description = __doc__

    def parse(self, report, objects, datas, localcontext):
        pool = Pool()
        account_obj = pool.get('account.account')
        period_obj = pool.get('account.period')
        company_obj = pool.get('company.company')

        company = company_obj.browse(datas['form']['company'])

        args = [
            ('company', '=', datas['form']['company']),
            ('kind', '!=', 'view'),
            ('fiscalyear', '=', datas['form']['fiscalyear']),
            ]

        if datas['form']['range_accounts'] == 'selected':
            account_range = []
            if datas['form']['accounts']:
                account_range = datas['form']['accounts'][0][1]
            args.append(('id', 'in', account_range))

        account_ids = account_obj.search(args, order=[
            ('code', 'ASC'), ('id', 'ASC')])

        start_period_ids = [0]
        start_period = None
        if datas['form']['start_period']:
            start_period = period_obj.browse(datas['form']['start_period'])
            start_period_ids = period_obj.search([
                ('fiscalyear', '=', datas['form']['fiscalyear']),
                ('end_date', '<=', start_period.start_date),
                ])

        with Transaction().set_context(
                fiscalyear=datas['form']['fiscalyear'],
                periods=start_period_ids,
                posted=datas['form']['posted']):
            start_accounts = account_obj.browse(account_ids)
        id2start_account = {}
        for account in start_accounts:
            id2start_account[account.id] = account

        end_period_ids = []
        if datas['form']['end_period']:
            end_period = period_obj.browse(datas['form']['end_period'])
            end_period_ids = period_obj.search([
                ('fiscalyear', '=', datas['form']['fiscalyear']),
                ('end_date', '<=', end_period.start_date),
                ])
            if datas['form']['end_period'] not in end_period_ids:
                end_period_ids.append(datas['form']['end_period'])
        else:
            end_period_ids = period_obj.search([
                ('fiscalyear', '=', datas['form']['fiscalyear']),
                ])

        with Transaction().set_context(
                    fiscalyear=datas['form']['fiscalyear'],
                    periods=end_period_ids,
                    posted=datas['form']['posted']):
            end_accounts = account_obj.browse(account_ids)
        id2end_account = {}
        for account in end_accounts:
            id2end_account[account.id] = account

        periods = period_obj.browse(end_period_ids)
        periods.sort(lambda x, y: cmp(x.start_date, y.start_date))
        localcontext['start_period'] = start_period or periods[0]
        periods.sort(lambda x, y: cmp(x.end_date, y.end_date))
        localcontext['end_period'] = periods[-1]

        if not datas['form']['empty_account']:
            to_remove = []
            for account_id in account_ids:
                if not self.get_line_ids(account_id, end_period_ids,
                        datas['form']['posted']):
                    to_remove.append(account_id)
            for account_id in to_remove:
                account_ids.remove(account_id)

        localcontext['accounts'] = account_obj.browse(account_ids)
        localcontext['id2start_account'] = id2start_account
        localcontext['id2end_account'] = id2end_account
        localcontext['digits'] = company.currency.digits
        localcontext['lines'] = lambda account_id: self.lines(account_id,
                list(set(end_period_ids).difference(
                    set(start_period_ids))), datas['form']['posted'])
        localcontext['company'] = company

        return super(GeneralLegderFiscalyear, self).parse(report, objects,
                datas, localcontext)

    def get_line_ids(self, account_id, period_ids, posted):
        move_line_obj = Pool().get('account.move.line')
        clause = [
            ('account', '=', account_id),
            ('period', 'in', period_ids),
            ('state', '!=', 'draft'),
            ]
        if posted:
            clause.append(('move.state', '=', 'posted'))
        return move_line_obj.search(clause)

    def lines(self, account_id, period_ids, posted):
        move_line_obj = Pool().get('account.move.line')
        move_line_ids = self.get_line_ids(account_id, period_ids, posted)
        move_lines = move_line_obj.browse(move_line_ids)
        move_lines.sort(lambda x, y: cmp(x.date, y.date))
        return move_lines

GeneralLegderFiscalyear()


class PrintTrialBalanceInit(ModelView):
    'Print Trial Balance Init'
    _name = 'account.account.print_trial_balance.init'
    _description = __doc__
    period = fields.Many2One('account.period', 'Period', domain=[
                    ('fiscalyear', If(Bool(Eval('fiscalyear')), '=', '!='),
                        Eval('fiscalyear')),
            ],
            states={
                    'required': Not(Bool(Eval('fiscalyear'))),
            },
            on_change=['fiscalyear', 'period'], depends=['fiscalyear'],
            help='Choose a period to create a report for a distinct period.')

    def __init__(self):
        super(PrintTrialBalanceInit, self).__init__()

        self.fiscalyear = copy.copy(self.fiscalyear)
        if self.fiscalyear.required is True:
            self.fiscalyear.required = False

        if self.fiscalyear.on_change is None:
            self.fiscalyear.on_change = ['fiscalyear', 'period']
        elif 'fiscalyear' not in self.fiscalyear.on_change:
            self.fiscalyear.on_change += ['fiscalyear']
        elif 'period' not in self.fiscalyear.on_change:
            self.fiscalyear.on_change += ['period']

        if self.fiscalyear.states is None:
            self.fiscalyear.states = {
                    'required': Not(Bool(Eval('period'))),
                }
        elif 'required'  in self.fiscalyear.states:
            self.fiscalyear.states['required'] = Or(
                    self.fiscalyear.states['required'],
                    Not(Bool(Eval('period'))))
        else:
            self.fiscalyear.states['required'] = Not(Bool(Eval('period')))
        if 'period' not in self.fiscalyear.depends:
            self.fiscalyear.depends = copy.copy(self.fiscalyear.depends)
            self.fiscalyear.depends.append('period')

        self.fiscalyear.help = 'Choose a fiscal year to create a report ' \
                'for a complete fiscal year.'

        self._reset_columns()

    def default_company(self):
        # TODO: Cleanup upstream, too
        return Transaction().context.get('company', False)

    def default_fiscalyear(self):
        return None

    def on_change_fiscalyear(self, vals):
        res = {}
        res['period'] = None
        return res

    def on_change_period(self, vals):
        period_obj = Pool().get('account.period')
        res = {}
        period_id = vals.get('period', False)
        res['fiscalyear'] = False
        if period_id:
            period = period_obj.browse(period_id)
            res['fiscalyear'] = period.fiscalyear.id

        return res

PrintTrialBalanceInit()


class PrintTrialBalanceFiscalyear(Wizard):
    'Print Trial Balance Fiscalyear'
    _name = 'account.account.print_trial_balance.fiscalyear'
    _description = __doc__

    states = PrintTrialBalance.states

    def __init__(self):
        super(PrintTrialBalanceFiscalyear, self).__init__()
        self.states = copy.copy(self.states)
        self.states['print']['result']['report'] = \
                'account.account.trial_balance.fiscalyear'

PrintTrialBalanceFiscalyear()


class TrialBalanceFiscalyear(Report):
    'Trial Balance Fiscalyear'
    _name = 'account.account.trial_balance.fiscalyear'
    _description = __doc__

    def parse(self, report, objects, datas, localcontext):
        pool = Pool()
        account_obj = pool.get('account.account')
        period_obj = pool.get('account.period')
        fiscalyear_obj = pool.get('account.fiscalyear')
        company_obj = pool.get('company.company')
        move_line_obj = pool.get('account.move.line')

        empty_account = datas['form']['empty_account']
        company = company_obj.browse(datas['form']['company'])
        fiscalyear_id = datas['form'].get('fiscalyear', False)

        period_id = datas['form'].get('period', False)
        if period_id:
            period = period_obj.browse(period_id)
            fiscalyear = period.fiscalyear
            period_ids = period_obj.search([
                    ('fiscalyear.id', '=', fiscalyear_id),
                    ('end_date', '<=', period.end_date)
                    ])
        else:
            # Yearly report == report of the last period of a fiscal year
            fiscalyear = fiscalyear_obj.browse(fiscalyear_id)
            period_ids = period_obj.search([
                    ('fiscalyear.id', '=', fiscalyear_id),
                    ], order=[('end_date', 'DESC')])
            if period_ids:
                period = period_obj.browse(period_ids[0])

        first_period_ids = period_obj.search([
                    ('fiscalyear.id', '=', fiscalyear_id),
                ], limit=1, order=[('start_date', 'ASC')])
        if first_period_ids:
            first_period = period_obj.browse(first_period_ids[0])

        # select accounts in move lines for fiscal year:
        move_line_account_list = move_line_obj.search_read([
                    ('date', '<=', period.end_date),
                    ('date', '>=', first_period.start_date),
                ], fields_names=['account'])
        move_line_account_ids = [i['account'] for i in move_line_account_list]
        used_account_ids = sorted(
                set(move_line_account_ids),
                key=lambda id_: account_obj.browse(id_).code)

        # select all accounts that are not used in the report year
        # it is needed to display accounts with only a starting balance
        not_used_account_ids = account_obj.search([
                ('fiscalyear', '=', fiscalyear.id),
                ('id', 'not in', used_account_ids),
                ('kind', '!=', 'view'),
                ('deferral', '=', True)
                ])
        if not not_used_account_ids:
            not_used_account_ids = []

        with Transaction().set_context(
                fiscalyear=fiscalyear_id,
                posted=datas['form']['posted'],
                periods=period_ids):
            accum_amounts =  account_obj.get_credit_debit(used_account_ids,
                    ['debit', 'credit'])

        localcontext['total_starting_balance_debit'] =  Decimal('0.0')
        localcontext['total_starting_balance_credit'] =  Decimal('0.0')
        localcontext['total_debit'] = Decimal('0.0')
        localcontext['total_credit'] = Decimal('0.0')
        localcontext['total_accum_debit'] = Decimal('0.0')
        localcontext['total_accum_credit'] = Decimal('0.0')
        localcontext['total_balance_debit'] = Decimal('0.0')
        localcontext['total_balance_credit'] = Decimal('0.0')

        # Create account class structure
        localcontext['account_classes'] = {}

        ctx2 = localcontext.copy()
        ctx2['fiscalyear'] = fiscalyear_id
        ctx2['posted'] = datas['form']['posted']
        ctx3 = ctx2.copy()
        if period_id:
            ctx2['periods'] = [period_id]
        else:
            ctx2['periods'] = period_ids

        report_account_ids = sorted(
                set(used_account_ids + not_used_account_ids),
                key=lambda id_: account_obj.browse(id_).code)
        report_accounts = account_obj.browse(report_account_ids)

        with Transaction().set_context(**ctx3):
            starting_balances = account_obj.get_starting_balance(
                    [x.id for x in report_accounts], None)

        with Transaction().set_context(**ctx2):
            period_debit_credits = account_obj.get_credit_debit(
                    [x.id for x in report_accounts], ['debit', 'credit'])

        for account in report_accounts:
            starting_balance = starting_balances[account.id]
            debit = Decimal('0.0')
            credit = Decimal('0.0')
            accum_debit = Decimal('0.0')
            accum_credit = Decimal('0.0')
            if account.id in not_used_account_ids and \
                    starting_balances[account.id] == Decimal('0.0') and \
                    not empty_account:
                continue
            if account.id in used_account_ids:
                debit = period_debit_credits['debit'][account.id]
                credit = period_debit_credits['credit'][account.id]
                accum_debit = accum_amounts['debit'][account.id]
                accum_credit = accum_amounts['credit'][account.id]

            account_class = self._get_account_class(account.code)
            localcontext['account_classes'].setdefault(account_class, {
                        'accounts': [],
                        'starting_balance_debit':  Decimal('0.0'),
                        'starting_balance_credit':  Decimal('0.0'),
                        'debit': Decimal('0.0'),
                        'credit': Decimal('0.0'),
                        'accum_debit': Decimal('0.0'),
                        'accum_credit': Decimal('0.0'),
                        'balance_debit': Decimal('0.0'),
                        'balance_credit': Decimal('0.0'),
                    })
            context_class = localcontext['account_classes'][account_class]

            if starting_balances[account.id] < 0:
                localcontext['total_starting_balance_credit'] += \
                        starting_balances[account.id]
                context_class['starting_balance_credit'] += \
                        starting_balances[account.id]
            else:
                localcontext['total_starting_balance_debit'] += \
                        starting_balances[account.id]
                context_class['starting_balance_debit'] += \
                        starting_balances[account.id]

            balance = starting_balances[account.id] + accum_debit - \
                    accum_credit

            localcontext['total_debit'] += debit
            localcontext['total_credit'] += credit
            localcontext['total_accum_debit'] += accum_debit
            localcontext['total_accum_credit'] += accum_credit

            context_class['accounts'].append({
                        'id': account.id,
                        'code': account.code or '',
                        'name': account.name,
                        'starting_balance': starting_balance,
                        'debit': debit,
                        'credit': credit,
                        'accum_debit': accum_debit,
                        'accum_credit': accum_credit,
                        'balance': balance,
                    })

            if balance < 0:
                localcontext['total_balance_credit'] += balance
                context_class['balance_credit'] += balance
            else:
                localcontext['total_balance_debit'] += balance
                context_class['balance_debit'] += balance

            context_class['debit'] += debit
            context_class['credit'] += credit
            context_class['accum_debit'] += accum_debit
            context_class['accum_credit'] += accum_credit

        localcontext['period'] = False
        if period_id:
            localcontext['period'] = period

        localcontext['fiscalyear'] = fiscalyear
        localcontext['company'] = company
        return super(TrialBalanceFiscalyear, self).parse(report, objects,
                datas, localcontext)

    def _get_account_class(self, code=False):
        '''Method which tries to find out an account class with the code
           of an account. Returns the first letter of code or ''.'''
        account_class = ''
        if code:
            account_class = str(code[0:1])
        return account_class


TrialBalanceFiscalyear()


class OpenThirdPartyBalance(Wizard):
    'Open Third Party Balance Fiscalyear'
    _name = 'account.account.open_third_party_balance.fiscalyear'
    _description = __doc__

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'account.account.open_third_party_balance.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('print', 'Print', 'tryton-ok', True),
                ],
            },
        },
        'print': {
            'result': {
                'type': 'print',
                'report': 'account.account.third_party_balance.fiscalyear',
                'state': 'end',
            },
        },
    }

OpenThirdPartyBalance()


class ThirdPartyBalanceFiscalyear(Report):
    'Third Party Balance Fiscalyear'
    _name = 'account.account.third_party_balance.fiscalyear'
    _description = __doc__

    def parse(self, report, objects, datas, localcontext):
        pool = Pool()
        party_obj = pool.get('party.party')
        move_line_obj = pool.get('account.move.line')
        company_obj = pool.get('company.company')
        date_obj = pool.get('ir.date')
        fiscalyear_obj = pool.get('account.fiscalyear')

        fiscalyear = fiscalyear_obj.browse(datas['form']['fiscalyear'])
        company = company_obj.browse(datas['form']['company'])
        localcontext['company'] = company
        localcontext['digits'] = company.currency.digits
        localcontext['fiscalyear'] = fiscalyear
        line_query, _ = move_line_obj.query_get()
        if datas['form']['posted']:
            posted_clause = "AND m.state = 'posted' "
        else:
            posted_clause = ""

        cursor = Transaction().cursor
        cursor.execute('SELECT l.party, SUM(l.debit), SUM(l.credit) ' \
                'FROM account_move_line l ' \
                    'JOIN account_move m ON (l.move = m.id) '
                    'JOIN account_account a ON (l.account = a.id) '
                'WHERE l.party IS NOT NULL '\
                    'AND a.active ' \
                    'AND a.kind IN (\'payable\',\'receivable\') ' \
                    'AND l.reconciliation IS NULL ' \
                    'AND a.company = %s ' \
                    'AND a.fiscalyear = %s ' \
                    'AND (l.maturity_date <= %s ' \
                        'OR l.maturity_date IS NULL) '\
                    'AND ' + line_query + ' ' \
                    + posted_clause + \
                'GROUP BY l.party ' \
                'HAVING (SUM(l.debit) != 0 OR SUM(l.credit) != 0)',
                (datas['form']['company'], fiscalyear.id, date_obj.today()))

        res = cursor.fetchall()
        id2party = {}
        for party in party_obj.browse([x[0] for x in res]):
            id2party[party.id] = party
        objects = [{
            'name': id2party[x[0]].rec_name,
            'debit': x[1],
            'credit': x[2],
            'solde': x[1] - x[2],
            } for x in res]
        objects.sort(lambda x, y: cmp(x['name'], y['name']))
        localcontext['total_debit'] = sum((x['debit'] for x in objects))
        localcontext['total_credit'] = sum((x['credit'] for x in objects))
        localcontext['total_solde'] = sum((x['solde'] for x in objects))

        return super(ThirdPartyBalanceFiscalyear, self).parse(report, objects,
                datas, localcontext)

ThirdPartyBalanceFiscalyear()


class OpenAgedBalanceFiscalyear(Wizard):
    'Open Aged Party Balance Fiscalyear'
    _name = 'account.account.open_aged_balance.fiscalyear'
    _description = __doc__

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'account.account.open_aged_balance.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('print', 'Print', 'tryton-ok', True),
                ],
            },
        },
        'print': {
            'actions': ['check',],
            'result': {
                'type': 'print',
                'report': 'account.account.aged_balance.fiscalyear',
                'state': 'end',
            },
        },
    }

    def __init__(self):
        super(OpenAgedBalanceFiscalyear, self).__init__()
        self._error_messages.update({
                'warning': 'Warning',
                'term_overlap_desc': 'You cannot define overlapping terms'})

    def check(self, datas):
        if not (datas['form']['term1'] < datas['form']['term2'] \
                  < datas['form']['term3']):
            self.raise_user_error(error="warning",
                    error_description="term_overlap_desc")
        return datas['form']

OpenAgedBalanceFiscalyear()


class AgedBalanceFiscalyear(Report):
    'Aged Balance Fiscalyear'
    _name = 'account.account.aged_balance.fiscalyear'
    _description = __doc__

    def parse(self, report, objects, datas, localcontext):
        pool = Pool()
        party_obj = pool.get('party.party')
        move_line_obj = pool.get('account.move.line')
        company_obj = pool.get('company.company')
        date_obj = pool.get('ir.date')
        fiscalyear_obj = pool.get('account.fiscalyear')

        fiscalyear = fiscalyear_obj.browse(datas['form']['fiscalyear'])
        args = [
            ('end_date', '<=', fiscalyear.end_date)
            ]
        fiscalyear_ids = fiscalyear_obj.search(args)

        company = company_obj.browse(datas['form']['company'])
        localcontext['digits'] = company.currency.digits
        localcontext['fiscalyear'] = fiscalyear_ids
        localcontext['posted'] = datas['form']['posted']
        line_query, _ = move_line_obj.query_get()

        terms = (datas['form']['term1'],
                  datas['form']['term2'],
                  datas['form']['term3'])
        if datas['form']['unit'] == 'month':
            coef = 30
        else:
            coef = 1

        kind = {'both': ('payable','receivable'),
                'supplier': ('payable',),
                'customer': ('receivable',),
                }[datas['form']['balance_type']]

        res = {}
        for position,term in enumerate(terms):
            if position == 0:
                term_query = '(l.maturity_date <= %s '\
                    'OR l.maturity_date IS NULL) '
                term_args = (date_obj.today() + \
                    datetime.timedelta(days=term*coef),)
            else:
                term_query = '(l.maturity_date <= %s '\
                    'AND l.maturity_date > %s) '
                term_args = (
                    date_obj.today() + datetime.timedelta(days=term*coef),
                    date_obj.today() + \
                        datetime.timedelta(days=terms[position-1]*coef),
                    )
            cursor = Transaction().cursor
            cursor.execute('SELECT l.party, SUM(l.debit) - SUM(l.credit) ' \
                    'FROM account_move_line l ' \
                        'JOIN account_move m ON (l.move = m.id) '
                        'JOIN account_account a ON (l.account = a.id) '
                    'WHERE l.party IS NOT NULL '\
                        'AND a.active ' \
                        'AND a.kind IN ('+ ','.join(('%s',) * len(kind)) + ") "\
                        'AND l.reconciliation IS NULL ' \
                        'AND l.move NOT IN (' \
                            # Sort out opening balance moves
                            'SELECT id FROM account_move m ' \
                            'WHERE m.journal IN (' \
                                'SELECT id FROM account_journal j ' \
                                "WHERE j.type = 'situation') " \
                                'AND m.period NOT IN (' \
                                    # but not opening balance moves of the first
                                    # fiscal year
                                    'SELECT id from account_period ' \
                                    'WHERE fiscalyear IN (' \
                                        'SELECT id from account_fiscalyear f ' \
                                        'ORDER BY f.start_date ASC LIMIT 1)))' \
                        'AND a.company = %s ' \
                        'AND '+ term_query+\
                        'AND ' + line_query + ' ' \
                    'GROUP BY l.party ' \
                    'HAVING (SUM(l.debit) - SUM(l.credit) != 0)',
                    kind + (datas['form']['company'],) + term_args)
            for party, solde in cursor.fetchall():
                if party in res:
                    res[party][position] = solde
                else:
                    res[party] = [(i[0] == position) and solde \
                            or Decimal("0.0") for i in enumerate(terms)]
        party_ids = party_obj.search([
            ('id', 'in', [k for k in res.iterkeys()]),
            ])
        parties = party_obj.browse(party_ids)

        localcontext['main_title'] = datas['form']['balance_type']
        localcontext['unit'] = datas['form']['unit']
        for i in range(3):
            localcontext['total' + str(i)] = sum((v[i] for v in res.itervalues()))
            localcontext['term' + str(i)] = terms[i]

        localcontext['company'] = company
        localcontext['parties']= [{
            'name': p.rec_name,
            'amount0': res[p.id][0],
            'amount1': res[p.id][1],
            'amount2': res[p.id][2],
            } for p in parties]

        return super(AgedBalanceFiscalyear, self).parse(report, objects, datas,
                localcontext)

AgedBalanceFiscalyear()


class OpenAccountInit(ModelView):
    'Open Account Init'
    _name = 'account.account.open_account.init'
    _description = __doc__

    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            help='Leave empty for all open fiscal year', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    with_moves_only = fields.Boolean('Only accounts with moves')

    def default_company(self):
        return Transaction().context.get('company', False)

    def default_fiscalyear(self):
        fiscalyear_obj = Pool().get('account.fiscalyear')
        return fiscalyear_obj.get_fiscalyear_id()

    def default_with_moves_only(self):
        return True

OpenAccountInit()


class OpenAccount(Wizard):
    'Open Account'
    _name = 'account.account.open_account'
    _description = __doc__

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'account.account.open_account.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('open', 'Open', 'tryton-ok', True),
                ],
            },
        },
        'open': {
            'result': {
                'type': 'action',
                'action': '_action_open_account',
                'state': 'end',
            },
        },
    }

    def _action_open_account(self, data):
        model_data_obj = Pool().get('ir.model.data')
        act_window_obj = Pool().get('ir.action.act_window')

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_account_form_moves'),
            ('module', '=', 'account_timeline'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        res['pyson_context'] = PYSONEncoder().encode({
            'fiscalyear': data['form']['fiscalyear'],
            'company': data['form']['company'],
            })
        if data['form']['with_moves_only']:
            domain = PYSONEncoder().encode([('move_lines', '=', True)])
            res['pyson_domain'] = res['pyson_domain'][:-1] + ', ' + domain[1:-1] + ']'
        return res

OpenAccount()

class AccountReport(Report):
    _name = 'account.account.report'

    def parse(self, report, objects, datas, localcontext):
        user_obj = Pool().get('res.user')
        user = user_obj.browse(Transaction().user)
        company = user.company
        account = objects[0]
        period = account.fiscalyear
        localcontext['fiscalyear'] = account.fiscalyear
        localcontext['company'] = company
        localcontext['company_name'] = company.name
        localcontext['period_name'] = period.name
        localcontext['period_start_date'] = period.start_date
        localcontext['period_end_date'] = period.end_date
        localcontext['acc_info'] = []
        for account in objects:
            starting_balance = account.starting_balance
            total_debit = account.debit
            total_credit = account.credit

            localcontext['acc_info'].append({
                        'total_debit': total_debit,
                        'total_credit': total_credit,
                        'total_balance': starting_balance + total_debit \
                                         - total_credit,
                        'starting_balance': starting_balance,
                    })
        return super(AccountReport, self).parse(report, objects, datas,
                localcontext)

AccountReport()
