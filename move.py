#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
import datetime
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Eval, PYSONEncoder
from trytond.transaction import Transaction
from trytond.pool import Pool


class Move(ModelSQL, ModelView):
    _name = 'account.move'

    def __init__(self):
        super(Move, self).__init__()
        self._constraints += [
            ('check_sign_lines', 'lines_all_one_sign'),
            ('check_journal_opening_balance', 'journal_opening_balance'),
        ]
        self._error_messages.update({
            'lines_all_one_sign': 'Either all move lines must all have a '
                'positive amount or a negative amount!',
            'journal_opening_balance': 'Opening balance accounts can only be '
                "used in moves for journals of type 'situation' and they are "
                'mandatory for moves of this kind.',
            'post_previous_year_open': 'You can not post an opening balance '
                'move until the previous fiscal year has been closed!',
            'post_posted_move': 'You can not post moves that already have '
                'been posted.\n\nAffected moves:\n\n%s',
            })

    def check_sign_lines(self, ids):
        res = True
        _ZERO = Decimal('0.0')
        for move in self.browse(ids):
            sign = False
            for line in move.lines:
                if line.credit != _ZERO:
                    amount = line.credit
                elif line.debit != _ZERO:
                    amount = line.debit
                else:
                    continue
                if not sign:
                    sign = amount > _ZERO and 1 or -1
                if sign < 0 and amount > _ZERO or sign > 0 and amount < _ZERO:
                    res = False
                    break
        return res

    def check_journal_opening_balance(self, ids):
        move_line_obj = Pool().get('account.move.line')
        for move in self.browse(ids):
            if len(move.lines) < 2:
                continue

            opening_balance_allowed = False
            if move.journal.type == 'situation':
                opening_balance_allowed = True

            line_ids = move_line_obj.search([
                ('move', '=', move.id),
                ('account.opening_balance_account', '=', True),
                ])
            if (opening_balance_allowed and not line_ids or
                    not opening_balance_allowed and line_ids):
                return False
        return True

    def post(self, ids):
        fiscalyear_obj = Pool().get('account.fiscalyear')

        if isinstance(ids, (int, long)):
            ids = [ids]

        fiscalyear_ids = fiscalyear_obj.search([],
            order=[('start_date', 'ASC')])
        previous_years = {}
        for fiscalyear in fiscalyear_obj.browse(fiscalyear_ids):
            previous_years[fiscalyear.end_date + datetime.timedelta(days=1)] = \
                fiscalyear

        move_ids = self.search([
            ('id', 'in', ids),
            ('journal.type', '=', 'situation'),
            ])
        for move in self.browse(move_ids):
            previous_year = previous_years.get(
                move.period.fiscalyear.start_date)
            if not previous_year:
                continue
            if previous_year.state != 'close':
                self.raise_user_error('post_previous_year_open')
        move_ids = self.search([
            ('id', 'in', ids),
            ('state', '=', 'posted'),
            ])
        if move_ids:
            moves = self.browse(move_ids)
            self.raise_user_error('post_posted_move', error_args=(
                ','.join([x.name for x in moves]),))
        return super(Move, self).post(ids)

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['name'] = False
        return super(Move, self).copy(ids, default=default)

Move()


class Line(ModelSQL, ModelView):
    _name = 'account.move.line'

    fiscalyear = fields.Function(fields.Many2One('account.fiscalyear',
            'Fiscal Year', on_change_with=['date']),
            'get_fiscalyear')

    def __init__(self):
        super(Line, self).__init__()

        self._constraints += [
            ('check_amount_lines', 'line_amount_vs_move_line_amount'),
        ]

        self.account = copy.copy(self.account)
        domain_account = ('fiscalyear', '=', Eval('fiscalyear'))
        if self.account.domain is None:
            self.account.domain = [domain_account]
        else:
            if PYSONEncoder().encode(domain_account) not in \
                    PYSONEncoder().encode(self.account.domain):
                self.account.domain += [domain_account]

        if self.account.depends is None:
            self.account.depends = ['fiscalyear']
        else:
            self.account.depends += ['fiscalyear']

        self.party = copy.copy(self.party)
        if self.party.on_change is None:
            self.party.on_change = ['move', 'party', 'account', 'debit',
                'credit', 'journal', 'date']
        elif 'date' not in self.party.on_change:
            self.party.on_change += ['date']

        self._error_messages.update({
            'no_fiscalyear': 'No fiscalyear found for this date. ' \
                    'Please create one.',
            'line_amount_vs_move_line_amount': 'The amount of a tax line must '
                    'have the same absolute value as the amount of the move '
                    'line!',
            })

        self._reset_columns()

    def default_fiscalyear(self):
        pool = Pool()
        date_obj = pool.get('ir.date')
        fiscalyear_obj = pool.get('account.fiscalyear')
        period_obj = pool.get('account.period')

        date = date_obj.today()
        line_ids = self.search([
            ('journal', '=', Transaction().context.get('journal', False)),
            ('period', '=', Transaction().context.get('period', False)),
            ], order=[('id', 'DESC')], limit=1)
        if line_ids:
            line = self.browse(line_ids[0])
            date = line.date
        elif Transaction().context.get('period'):
            period = period_obj.browse(Transaction().context['period'])
            date = period.start_date
        if Transaction().context.get('date'):
            date = Transaction().context['date']
        return fiscalyear_obj.get_fiscalyear_id(date=date)

    def on_change_with_fiscalyear(self, vals):
        fiscalyear_obj = Pool().get('account.fiscalyear')

        res = False
        if vals.get('date'):
            res = fiscalyear_obj.get_fiscalyear_id(date=vals['date'])
            if not res:
                self.raise_user_error('no_fiscalyear')
        return res

    def get_fiscalyear(self, ids, name):
        fiscalyear_obj = Pool().get('account.fiscalyear')
        res = {}
        for line in self.browse(ids):
            res[line.id] = fiscalyear_obj.get_fiscalyear_id(date=line.date)
        return res

    def on_change_party(self, vals):
        # TODO: Why is this needed?
        with Transaction().set_context(effective_date=vals.get('date', False)):
            return super(Line, self).on_change_party(vals)

    def default_get(self, fields, with_rec_name=True):
        values = super(Line, self).default_get(fields,
                with_rec_name=with_rec_name)
        with Transaction().set_context(effective_date=values.get('date', False)):
            return super(Line, self).default_get(fields,
                    with_rec_name=with_rec_name)

    def query_get(self, obj='l'):
        '''
        Return SQL clause and fiscal years for account move line
        depending of the context.

        :param obj: the SQL alias of account_move_line in the query
        :return: a tuple with the SQL clause and the fiscalyear ids
        '''
        fiscalyear_obj = Pool().get('account.fiscalyear')

        if Transaction().context.get('date'):
            fiscalyear_ids = fiscalyear_obj.search([
                ('start_date', '<=', Transaction().context['date']),
                ('end_date', '>=', Transaction().context['date']),
                ], limit=1)
            if not fiscalyear_ids:
                fiscalyear_ids = [0]
            if Transaction().context.get('posted'):
                return (obj + '.active ' \
                        'AND ' + obj + '.state != \'draft\' ' \
                        'AND ' + obj + '.move IN (' \
                            'SELECT m.id FROM account_move AS m, ' \
                                'account_period AS p ' \
                                'WHERE m.period = p.id ' \
                                    'AND p.fiscalyear = ' + \
                                        str(fiscalyear_ids[0]) + ' ' \
                                    'AND m.date <= date(\'' + \
                                        str(Transaction().context['date']) +
                                        '\') ' \
                                    'AND m.state = \'posted\' ' \
                            ')', fiscalyear_ids)
            else:
                return (obj + '.active ' \
                        'AND ' + obj + '.state != \'draft\' ' \
                        'AND ' + obj + '.move IN (' \
                            'SELECT m.id FROM account_move AS m, ' \
                                'account_period AS p ' \
                                'WHERE m.period = p.id ' \
                                    'AND p.fiscalyear = ' + \
                                        str(fiscalyear_ids[0]) + ' ' \
                                    'AND m.date <= date(\'' + \
                                        str(Transaction().context['date']) +
                                        '\')' \
                            ')', fiscalyear_ids)

        if not Transaction().context.get('fiscalyear', False):
            fiscalyear_ids = fiscalyear_obj.search([])
            fiscalyear_clause = (','.join(map(str, fiscalyear_ids))) or '0'
        elif isinstance(Transaction().context.get('fiscalyear'), (tuple, list)):
            fiscalyear_ids = Transaction().context['fiscalyear']
            fiscalyear_clause = (','.join(map(str, fiscalyear_ids))) or '0'
        else:
            fiscalyear_ids = [int(Transaction().context.get('fiscalyear'))]
            fiscalyear_clause = '%s' % int(Transaction().context.get(
                    'fiscalyear'))

        if Transaction().context.get('periods', False):
            ids = ','.join(str(int(x)) for x in Transaction().context['periods'])
            if Transaction().context.get('posted'):
                return (obj + '.active ' \
                        'AND ' + obj + '.state != \'draft\' ' \
                        'AND ' + obj + '.move IN (' \
                            'SELECT id FROM account_move ' \
                                'WHERE period IN (' + ids + ') ' \
                                    'AND state = \'posted\' ' \
                            ')', [])
            else:
                return (obj + '.active ' \
                        'AND ' + obj + '.state != \'draft\' ' \
                        'AND ' + obj + '.move IN (' \
                            'SELECT id FROM account_move ' \
                                'WHERE period IN (' + ids + ')' \
                            ')', [])
        else:
            if Transaction().context.get('posted'):
                return (obj + '.active ' \
                        'AND ' + obj + '.state != \'draft\' ' \
                        'AND ' + obj + '.move IN (' \
                            'SELECT id FROM account_move ' \
                                'WHERE period IN (' \
                                    'SELECT id FROM account_period ' \
                                    'WHERE fiscalyear IN (' + fiscalyear_clause + ')' \
                                    ') ' \
                                    'AND state = \'posted\' ' \
                            ')', fiscalyear_ids)
            else:
                return (obj + '.active ' \
                        'AND ' + obj + '.state != \'draft\' ' \
                        'AND ' + obj + '.move IN (' \
                            'SELECT id FROM account_move ' \
                                'WHERE period IN (' \
                                    'SELECT id FROM account_period ' \
                                    'WHERE fiscalyear IN (' + fiscalyear_clause + ')' \
                                ')' \
                            ')', fiscalyear_ids)

    def check_amount_lines(self, ids):
        res = True
        for line in self.browse(ids):
            if not line.tax_lines:
                continue
            if not self._check_amount_line(line):
                res = False
                break
        return res

    def _check_amount_line(self, line):
        res = True
        _ZERO = Decimal('0.0')
        if line.credit != _ZERO:
            amount = line.credit
        elif line.debit != _ZERO:
            amount = line.debit
        else:
            return res
        amount_tax_lines = Decimal('0.0')
        for tax_line in line.tax_lines:
            amount_tax_lines += tax_line.amount
        if abs(amount_tax_lines) != abs(amount):
            res = False
        return res

Line()


class Reconciliation(ModelSQL, ModelView):
    _name = 'account.move.reconciliation'

    def check_lines(self, ids):
        currency_obj = Pool().get('currency.currency')
        for reconciliation in self.browse(ids):
            amount = Decimal('0.0')
            account = None
            party = None
            for line in reconciliation.lines:
                if line.state != 'valid':
                    return False
                amount += line.debit - line.credit
                if not account:
                    account = line.account
                elif account.id != line.account.id:
                    siblings = account.all_predecessors
                    siblings += account.all_successors
                    if line.account not in siblings:
                        return False
                if not line.account.reconcile:
                    return False
                if not party:
                    party = line.party
                elif line.party and party.id != line.party.id and \
                        account.party_is_mandatory:
                    return False
            if not currency_obj.is_zero(account.company.currency,
                    amount):
                return False
        return True

Reconciliation()


class OpenReconcileLinesInit(ModelView):
    _name = 'account.move.open_reconcile_lines.init'

    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            readonly=True)

    def default_fiscalyear(self):
        pool = Pool()
        fiscalyear_obj = pool.get('account.fiscalyear')
        date_obj = pool.get('ir.date')
        date = date_obj.today()
        return fiscalyear_obj.get_fiscalyear_id(date=date)

    def __init__(self):
        super(OpenReconcileLinesInit, self).__init__()
        self.account = copy.copy(self.account)
        if self.account.domain is None:
            self.account.domain = [
                ('kind', '!=', 'view'),
                ('reconcile', '=', True),
                ('fiscalyear', '=', Eval('fiscalyear'))]
        else:
            self.account.domain += [('fiscalyear', '=', Eval('fiscalyear'))]
        if self.account.depends is None:
            self.account.depends = ['fiscalyear']
        elif 'fiscalyear' not in self.account.depends:
            self.account.depends += ['fiscalyear']
        self._reset_columns()

OpenReconcileLinesInit()


class OpenReconcileLines(Wizard):
    _name = 'account.move.open_reconcile_lines'

    def _action_open_reconcile_lines(self, data):
        pool = Pool()
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')
        account_obj = pool.get('account.account')
        fiscalyear_obj = pool.get('account.fiscalyear')

        first_fiscalyear_ids = fiscalyear_obj.search([], limit=1,
            order=[('start_date', 'ASC')])

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_move_line_form'),
            ('module', '=', 'account'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        account = account_obj.browse(data['form']['account'])
        res = act_window_obj.read(model_data.db_id)
        res['pyson_domain'] = PYSONEncoder().encode([
            ('account', 'in',
                [x.id for x in account.all_predecessors] + [account.id]),
            ('reconciliation', '=', False),
            ['OR',
                    ('move.journal.type', '!=', 'situation'),
                    ('move.period.fiscalyear', 'in', first_fiscalyear_ids),
                ]
            ])
        return res

OpenReconcileLines()
