#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Timeline',
    'name_de_DE': 'Buchhaltung Gültigkeitszeitraum',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Timeline for Accounting Periods
    - Provides accounting with temporal validity evaluation
''',
    'description_de_DE': '''Gültigkeitszeiträume für die Buchhaltung
    - Stellt die Bindung der Buchhaltung an Gültigkeitszeiträume bereit
''',
    'depends': [
        'account',
        'account_tax_code_type',
        'account_option_party_required',
        'account_tax_reverse_charge',
    ],
    'xml': [
        'account.xml',
        'configuration.xml',
        'fiscalyear.xml',
        'party.xml',
        'tax.xml',
        'journal.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
